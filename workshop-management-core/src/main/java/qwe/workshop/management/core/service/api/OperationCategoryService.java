package qwe.workshop.management.core.service.api;

import qwe.workshop.management.core.domain.operation.OperationCategory;

import javax.validation.Valid;
import java.util.List;

/**
 * User: artem
 * Date: 04.07.15
 * Time: 5:40
 */
public interface OperationCategoryService {

    public List<OperationCategory> findAll();

    public OperationCategory save(@Valid OperationCategory category);

    public void delete(long categoryId);

}
