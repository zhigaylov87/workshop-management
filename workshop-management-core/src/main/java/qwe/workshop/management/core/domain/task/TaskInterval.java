package qwe.workshop.management.core.domain.task;

import qwe.workshop.management.core.domain.Identifiable;
import qwe.workshop.management.core.util.ObjectUtils;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;

/**
 * User: artem
 * Date: 18.06.15
 * Time: 4:30
 */
@Entity
@Table(name = "TASK_INTERVAL")
public class TaskInterval implements Identifiable, Serializable {

    private Long id;
    private Date startTimestamp;
    private Date endTimestamp;
    private Task task;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @NotNull(message = "{TaskInterval.startTimestamp.NotNull}")
    @Column(name = "START_TIMESTAMP")
    @Temporal(TemporalType.TIMESTAMP)
    public Date getStartTimestamp() {
        return startTimestamp;
    }

    public void setStartTimestamp(Date startTimestamp) {
        this.startTimestamp = startTimestamp;
    }

    @NotNull(message = "{TaskInterval.endTimestamp.NotNull}")
    @Column(name = "END_TIMESTAMP")
    @Temporal(TemporalType.TIMESTAMP)
    public Date getEndTimestamp() {
        return endTimestamp;
    }

    public void setEndTimestamp(Date endTimestamp) {
        this.endTimestamp = endTimestamp;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "TASK_ID", nullable = false)
    public Task getTask() {
        return task;
    }

    public void setTask(Task task) {
        this.task = task;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof TaskInterval)) return false;

        TaskInterval that = (TaskInterval) o;
        return ObjectUtils.equalsIds(this, that);
    }

    @Override
    public int hashCode() {
        return id != null ? id.hashCode() : 0;
    }

    @Override
    public String toString() {
        return "TaskInterval {" +
                "id: " + id +
                ", startTimestamp: " + startTimestamp +
                ", endTimestamp: " + endTimestamp +
                ", task: " + task +
                '}';
    }
}
