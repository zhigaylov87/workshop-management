package qwe.workshop.management.core.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import qwe.workshop.management.core.domain.operation.OperationCategory;

import java.util.List;

/**
 * User: artem
 * Date: 04.07.15
 * Time: 4:01
 */
public interface OperationCategoryRepository extends CrudRepository<OperationCategory, Long> {

    @Query("select c from OperationCategory c order by c.type asc, c.name asc")
    public List<OperationCategory> findAll();

}
