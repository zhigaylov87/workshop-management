package qwe.workshop.management.web.util.exceptionhandler;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.faces.context.ExceptionHandler;
import javax.faces.context.FacesContext;

/**
 * Overriding only for logging to slf4j
 *
 * User: artem
 * Date: 14.11.13
 * Time: 0:30
 */
public class LoggingFullAjaxExceptionHandler extends org.omnifaces.exceptionhandler.FullAjaxExceptionHandler {

    private static final Logger log = LoggerFactory.getLogger(LoggingFullAjaxExceptionHandler.class);

    public LoggingFullAjaxExceptionHandler(ExceptionHandler wrapped) {
        super(wrapped);
    }

    @Override
    protected void logException(FacesContext context, Throwable exception, String location, String message, Object... parameters) {
        log.error(String.format(message, location), exception);
    }
}
