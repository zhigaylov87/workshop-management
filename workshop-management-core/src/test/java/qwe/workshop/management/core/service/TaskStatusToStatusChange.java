package qwe.workshop.management.core.service;

import qwe.workshop.management.core.domain.task.TaskStatus;

/**
 * User: artem
 * Date: 18.07.15
 * Time: 5:56
 */
public class TaskStatusToStatusChange {

    private TaskStatus currentStatus;
    private TaskStatus newStatus;

    public TaskStatusToStatusChange() {
    }

    public TaskStatusToStatusChange(TaskStatus currentStatus, TaskStatus newStatus) {
        this.currentStatus = currentStatus;
        this.newStatus = newStatus;
    }

    public TaskStatus getCurrentStatus() {
        return currentStatus;
    }

    public void setCurrentStatus(TaskStatus currentStatus) {
        this.currentStatus = currentStatus;
    }

    public TaskStatus getNewStatus() {
        return newStatus;
    }

    public void setNewStatus(TaskStatus newStatus) {
        this.newStatus = newStatus;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof TaskStatusToStatusChange)) return false;

        TaskStatusToStatusChange that = (TaskStatusToStatusChange) o;

        if (newStatus != that.newStatus) return false;
        if (currentStatus != that.currentStatus) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = currentStatus.hashCode();
        result = 31 * result + newStatus.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "TaskStatusToStatusChange {" +
                "currentStatus: " + currentStatus +
                ", newStatus: " + newStatus +
                '}';
    }

}
