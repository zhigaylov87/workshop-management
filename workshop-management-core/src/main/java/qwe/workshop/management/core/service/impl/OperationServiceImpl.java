package qwe.workshop.management.core.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import qwe.workshop.management.core.CoreException;
import qwe.workshop.management.core.domain.operation.Operation;
import qwe.workshop.management.core.repository.OperationRepository;
import qwe.workshop.management.core.service.api.OperationService;
import qwe.workshop.management.core.util.DateInterval;

import javax.validation.Valid;

/**
 * User: artem
 * Date: 04.07.15
 * Time: 7:15
 */
@Service("operationService")
@Repository
@Transactional
public class OperationServiceImpl implements OperationService {

    private static final Logger log = LoggerFactory.getLogger(OperationServiceImpl.class);

    @Autowired
    private OperationRepository operationRepository;

    @Transactional(readOnly = true)
    @Override
    public Page<Operation> find(Pageable pageable, DateInterval interval) {
        String logMsg = " of loading operations. Page number: " + pageable.getPageNumber() +
                ", page size: " + pageable.getPageSize() + ", interval: " + interval;
        try {
            log.info("Start" + logMsg);
            Page<Operation> page = operationRepository.find(pageable, interval.getStartDate(), interval.getEndDate());
            log.info("End" + logMsg + ". Total count: " + page.getTotalElements() + ", page count: " + page.getNumberOfElements());
            return page;
        } catch (Exception e) {
            log.error("Error" + logMsg, e);
            throw new CoreException(e);
        }
    }

    @Override
    public Operation save(@Valid Operation operation) {
        String logMsg = " of saving operation: " + operation;
        try {
            log.info("Start" + logMsg);
            operation = operationRepository.save(operation);
            log.info("End" + logMsg + ". Saved operation: " + operation);
            return operation;
        } catch (Exception e) {
            log.error("Error" + logMsg, e);
            throw new CoreException(e);
        }
    }

    @Override
    public void delete(long operationId) {
        String logMsg = " of deleting operation. Operation id: " + operationId;
        try {
            log.info("Start" + logMsg);
            // TODO: Нужны ли какие-нибудь проверки при удалении?
            Operation operation = operationRepository.findOne(operationId);
            log.info("Processing" + logMsg + ". Operation: " + operation);
            operationRepository.delete(operation);
            log.info("End" + logMsg);
        } catch (Exception e) {
            log.error("Error" + logMsg, e);
            throw new CoreException(e);
        }
    }
}
