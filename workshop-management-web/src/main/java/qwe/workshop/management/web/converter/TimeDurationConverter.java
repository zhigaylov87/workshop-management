package qwe.workshop.management.web.converter;

import qwe.workshop.management.core.domain.TimeDuration;
import qwe.workshop.management.core.util.FormatUtils;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

/**
 * User: artem
 * Date: 25.06.15
 * Time: 5:54
 */
@FacesConverter(forClass = TimeDuration.class)
public class TimeDurationConverter implements Converter {

    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String value) {
        throw new UnsupportedOperationException("getAsObject direction not supported");
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value) {
        if (!(value instanceof TimeDuration)) {
            throw new IllegalArgumentException("Value must be instance of " + TimeDuration.class + ". Value: " + value);
        }
        return FormatUtils.formatDuration((TimeDuration) value);
    }
}
