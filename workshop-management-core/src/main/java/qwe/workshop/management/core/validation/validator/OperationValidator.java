package qwe.workshop.management.core.validation.validator;

import qwe.workshop.management.core.CoreException;
import qwe.workshop.management.core.domain.operation.Operation;
import qwe.workshop.management.core.domain.operation.OperationType;
import qwe.workshop.management.core.validation.constraint.OperationConstraint;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 * User: artem
 * Date: 03.01.15
 * Time: 4:50
 */
public class OperationValidator implements ConstraintValidator<OperationConstraint, Operation> {

    @Override
    public void initialize(OperationConstraint constraintAnnotation) {
    }

    @Override
    public boolean isValid(Operation operation, ConstraintValidatorContext context) {
        context.disableDefaultConstraintViolation();

        if (operation.getCategory() != null) {
            OperationType type = operation.getCategory().getType();
            switch (type) {
                case EXPENSE:
                    if (operation.getOrderSource() != null) {
                        context.buildConstraintViolationWithTemplate("{Operation.orderSource.NotEmptyForExpense}").addConstraintViolation();
                        return false;
                    }
                    break;
                case INCOME:
                    if (operation.getOrderSource() == null) {
                        context.buildConstraintViolationWithTemplate("{Operation.orderSource.EmptyForIncome}").addConstraintViolation();
                        return false;
                    }
                    break;
                default:
                    throw new CoreException("Unknown operation type: " + type);
            }
        }

        return true;
    }
}
