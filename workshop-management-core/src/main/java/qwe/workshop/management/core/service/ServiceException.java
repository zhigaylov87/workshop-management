package qwe.workshop.management.core.service;

import org.springframework.context.MessageSourceResolvable;
import org.springframework.context.support.DefaultMessageSourceResolvable;

/**
 * User: artem
 * Date: 31.03.14
 * Time: 1:55
 */
public class ServiceException extends RuntimeException {

    private MessageSourceResolvable message;

    public ServiceException(String msgKey, Object... params) {
        super(msgKey);
        message = new DefaultMessageSourceResolvable(new String[]{msgKey}, params, msgKey);
    }

    public MessageSourceResolvable getMessageSourceResolvable() {
        return message;
    }

}
