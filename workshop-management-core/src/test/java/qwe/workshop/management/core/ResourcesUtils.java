package qwe.workshop.management.core;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.io.IOUtils;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

/**
 * User: artem
 * Date: 18.07.15
 * Time: 4:57
 */
public final class ResourcesUtils {
    private ResourcesUtils() {
    }

    /**
     * For example: <b>2015-07-13 19:53:51</b>
     */
    public static final String TIMESTAMP_PATTERN = "yyyy-MM-dd HH:mm:ss";


    private static final String ENCODING = "UTF-8";

    private static final ObjectMapper JSON_MAPPER = new ObjectMapper();


    public static <T> List<T> getList(JsonResource resource, Class<T> elementType) {
        try (InputStream is = ResourcesUtils.class.getResourceAsStream(resource.getPath())) {
            String json = IOUtils.toString(is, ENCODING);
            return JSON_MAPPER.readValue(json, JSON_MAPPER.getTypeFactory().constructCollectionType(List.class, elementType));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
