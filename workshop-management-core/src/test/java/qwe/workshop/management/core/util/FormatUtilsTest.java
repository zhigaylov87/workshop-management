package qwe.workshop.management.core.util;

import org.junit.Test;
import qwe.workshop.management.core.AbstractTest;
import qwe.workshop.management.core.ResourcesUtils;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static qwe.workshop.management.core.JsonResource.ValidFormatTimeDurations;

/**
 * User: artem
 * Date: 18.07.15
 * Time: 8:59
 */
public class FormatUtilsTest extends AbstractTest {

    @Test
    public void testFormatTimeDuration() {
        List<FormatTimeDuration> formatTimeDurations = ResourcesUtils.getList(ValidFormatTimeDurations, FormatTimeDuration.class);
        for (FormatTimeDuration formatTimeDuration : formatTimeDurations) {
            assertEquals("FormatTimeDuration: " + formatTimeDuration,
                    formatTimeDuration.getFormatted(),
                    FormatUtils.formatDuration(formatTimeDuration.getDuration()));
        }

    }

}
