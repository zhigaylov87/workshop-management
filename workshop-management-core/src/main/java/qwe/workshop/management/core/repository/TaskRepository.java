package qwe.workshop.management.core.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import qwe.workshop.management.core.domain.task.Task;

import java.util.List;

/**
 * User: artem
 * Date: 19.06.15
 * Time: 5:29
 */
public interface TaskRepository extends PagingAndSortingRepository<Task, Long> {

    @Query("select distinct t from Task t " +
            "join fetch t.type " +
            "left join fetch t.intervals " +
            "where t.status <> 'FINISHED' order by t.statusTimestamp desc")
    public List<Task> findCurrent();

    @Query(value = "select distinct t from Task t " +
            "join fetch t.type " +
            "left join fetch t.intervals " +
            "where t.status = 'FINISHED' order by t.statusTimestamp desc",
            countQuery = "select count(t) from Task t where t.status = 'FINISHED'")
    public Page<Task> findFinished(Pageable pageable);

    @Query("select t from Task t where t.status = 'ACTIVE'")
    public List<Task> findActive();

}
