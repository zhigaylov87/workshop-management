package qwe.workshop.management.core.util;

import javax.faces.context.FacesContext;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * User: artem
 * Date: 10.05.14
 * Time: 0:45
 */
public class WebUtils {

    public static Integer getIntegerRequestParameter(String parameterName) {
        String value = getRequestParameter(parameterName);
        return value == null ? null : Integer.valueOf(value);
    }

    public static Long getLongRequestParameter(String parameterName) {
        String value = getRequestParameter(parameterName);
        return value == null ? null : Long.valueOf(value);
    }

    public static String getRequestParameter(String parameterName) {
        Map<String, String> params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
        return params.get(parameterName);
    }

    public static List<Long> getLongListRequestParameter(String parameterName, String valuesDelimiter) {
        String valuesStr = getRequestParameter(parameterName);
        if (valuesStr == null) {
            return null;
        }

        List<Long> values = new ArrayList<Long>();
        for (String valueStr : valuesStr.split(valuesDelimiter)) {
            values.add(Long.valueOf(valueStr));
        }
        return values;
    }

}
