package qwe.workshop.management.web.bean.task;

import org.primefaces.context.RequestContext;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import qwe.workshop.management.core.domain.task.Task;
import qwe.workshop.management.core.domain.task.TaskStatus;
import qwe.workshop.management.core.domain.task.TaskType;
import qwe.workshop.management.core.service.ServiceException;
import qwe.workshop.management.core.service.api.TaskService;
import qwe.workshop.management.core.service.api.TaskTypeService;
import qwe.workshop.management.core.util.DateHelper;
import qwe.workshop.management.core.util.EnumHelper;
import qwe.workshop.management.web.component.EntityListEditor;
import qwe.workshop.management.web.converter.IdentifiableConverter;
import qwe.workshop.management.web.util.MessageManager;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.validation.ConstraintViolationException;
import java.io.Serializable;
import java.util.Arrays;
import java.util.List;

/**
 * User: artem
 * Date: 22.06.15
 * Time: 1:31
 */
@Component("taskPlanCurrentBean")
@Scope("view")
public class TaskPlanCurrentBean implements EntityListEditor<Task>, Serializable {

    private static final String BUNDLE_NAME = "qwe.workshop.management.web.i18n.task.task_plan_current";

    @Resource(name = "taskService")
    private transient TaskService taskService;

    @Resource(name = "taskTypeService")
    private transient TaskTypeService taskTypeService;

    @Resource(name = "dateHelper")
    private transient DateHelper dateHelper;

    @Resource(name = "enumHelper")
    private transient EnumHelper enumHelper;

    @Resource(name = "messageManager")
    private transient MessageManager messageManager;

    private List<Task> currentTasks;
    private Task preparedTask;
    private String editorDialogTitle;

    private List<TaskType> taskTypes;

    @PostConstruct
    private void init() {
        taskTypes = taskTypeService.findAll();
        reloadTasks();
    }

    @Override
    public void prepareToAdd() {
        preparedTask = new Task();
        preparedTask.setTaskDate(dateHelper.getCurrentDate());
        // TODO: По сути setStatus и setStatusTimestamp нужны для прохождения валидации в TaskService
        // TODO: как будет время запилить использование групп валидации.
        // TODO: Вместо javax.validation.Valid использовать org.springframework.validation.annotation.Validated
        preparedTask.setStatus(TaskStatus.PLANNED);
        preparedTask.setStatusTimestamp(dateHelper.getCurrentDate());

        editorDialogTitle = messageManager.getMessage(BUNDLE_NAME, "add_dialog_title");
    }

    @Override
    public void prepareToEdit(Task entity) {
        preparedTask = new Task(entity);
        editorDialogTitle = messageManager.getMessage(BUNDLE_NAME, "edit_dialog_title");
    }

    @Override
    public void prepareToDelete(Task entity) {
        preparedTask = entity;
        editorDialogTitle = messageManager.getMessage(BUNDLE_NAME, "delete_dialog_title", preparedTask.getName());
    }

    public void prepareToFinish(Task task) {
        preparedTask = task;
    }

    @Override
    public String getEditorDialogTitle() {
        return editorDialogTitle;
    }

    @Override
    public void save() {
        try {
            preparedTask = taskService.save(preparedTask);
        } catch (ConstraintViolationException e) {
            messageManager.addErrorMessage(e);
            FacesContext.getCurrentInstance().validationFailed();
            return;
        }

        messageManager.addInfoMessage(BUNDLE_NAME, "save_success", preparedTask.getName());
        reloadTasks();
        RequestContext.getCurrentInstance().update(Arrays.asList("entityListEditorTableForm", "messages"));
    }

    @Override
    public boolean canBeDelete(Task entity) {
        return taskService.canBeDelete(entity.getStatus());
    }

    @Override
    public void delete() {
        try {
            taskService.delete(preparedTask.getId());
        } catch (ServiceException e) {
            messageManager.addErrorMessage(e);
            return;
        }

        messageManager.addInfoMessage(BUNDLE_NAME, "delete_success", preparedTask.getName());
        reloadTasks();
    }

    public void finish() {
        changeTaskStatus(preparedTask, TaskStatus.FINISHED);
    }

    public void changeTaskStatus(Task task, TaskStatus newStatus) {
        taskService.changeStatus(task.getId(), newStatus);
        messageManager.addInfoMessage(BUNDLE_NAME, "change_status_success", task.getName(), enumHelper.getDescription(newStatus));
        reloadTasks();
    }

    private void reloadTasks() {
        currentTasks = taskService.findCurrent();
    }

    // setters & getters


    public List<Task> getCurrentTasks() {
        return currentTasks;
    }

    public List<TaskType> getTaskTypes() {
        return taskTypes;
    }

    public Converter getTaskTypeConverter() {
        return new IdentifiableConverter<>(taskTypes, true);
    }

    public Task getPreparedTask() {
        return preparedTask;
    }
}
