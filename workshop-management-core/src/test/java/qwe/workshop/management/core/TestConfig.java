package qwe.workshop.management.core;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;
import org.springframework.context.annotation.Profile;

/**
 * http://java.dzone.com/articles/why-i-spring-bean-aliasing
 *
 * User: artem
 * Date: 05.07.14
 * Time: 20:05
 */
@Configuration
@ImportResource("classpath:spring/test-context.xml")
@Profile("test")
public class TestConfig {



}
