package qwe.workshop.management.core.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import qwe.workshop.management.core.domain.operation.Operation;

import java.util.Date;

/**
 * User: artem
 * Date: 04.07.15
 * Time: 4:02
 */
public interface OperationRepository extends PagingAndSortingRepository<Operation, Long> {

    @Query(value = "select distinct op from Operation op " +
            "join fetch op.category " +
            "where op.date between :startDate and :endDate " +
            "order by op.date desc, op.category.type, op.amount desc",
            countQuery = "select count(op) from Operation op where op.date between :startDate and :endDate")
    public Page<Operation> find(Pageable pageable, @Param("startDate") Date startDate, @Param("endDate") Date endDate);

}
