package qwe.workshop.management.core.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import qwe.workshop.management.core.CoreException;
import qwe.workshop.management.core.domain.TimeDuration;
import qwe.workshop.management.core.domain.task.TaskType;
import qwe.workshop.management.core.report.task.TaskDurationByTypeReport;
import qwe.workshop.management.core.repository.TaskReportRepository;
import qwe.workshop.management.core.repository.TaskTypeRepository;
import qwe.workshop.management.core.service.api.TaskReportService;
import qwe.workshop.management.core.util.DateInterval;
import qwe.workshop.management.core.util.IdentifiableList;
import qwe.workshop.management.core.util.NumberUtils;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Arrays;
import java.util.List;

/**
 * User: artem
 * Date: 11.07.15
 * Time: 3:21
 */
@Service("taskReportService")
@Repository
@Transactional(readOnly = true)
public class TaskReportServiceImpl implements TaskReportService {

    private static final Logger log = LoggerFactory.getLogger(TaskReportServiceImpl.class);

    @Autowired
    private TaskReportRepository taskReportRepository;
    @Autowired
    private TaskTypeRepository taskTypeRepository;

    @Override
    public TaskDurationByTypeReport getDurationByTypeReport(DateInterval interval) {
        String logMsg = " of fetching duration by task type report. Interval: " + interval;
        try {
            log.info("Start" + logMsg);
            List<Object[]> typeIdToSeconds = taskReportRepository.byTypeReport(
                    interval.getStartDateBottom(), interval.getEndDateTop());
            IdentifiableList<TaskType> types = new IdentifiableList<>(taskTypeRepository.findAll());

            TaskDurationByTypeReport report = new TaskDurationByTypeReport();
            for (Object[] objects : typeIdToSeconds) {
                if (log.isDebugEnabled()) {
                    log.debug(Arrays.toString(objects));
                }
                long typeId = NumberUtils.toLong((BigInteger) objects[0]);
                long seconds = NumberUtils.toLong((BigDecimal) objects[1]);
                report.add(types.getById(typeId), new TimeDuration(seconds * 1000));
            }

            log.info("End" + logMsg + ". Report: " + report);
            return report;
        } catch (Exception e) {
            log.error("Error" + logMsg, e);
            throw new CoreException(e);
        }
    }
}