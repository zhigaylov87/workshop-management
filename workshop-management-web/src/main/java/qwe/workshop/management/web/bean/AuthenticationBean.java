package qwe.workshop.management.web.bean;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import qwe.workshop.management.web.util.MessageManager;

import javax.annotation.Resource;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;
import java.io.Serializable;

/**
 * User: artem
 * Date: 20.05.15
 * Time: 1:55
 */
@Component("authenticationBean")
@Scope("session")
public class AuthenticationBean implements Serializable {

    private static final String BUNDLE_NAME = "qwe.workshop.management.web.i18n.authentification";
    private static final String LOGIN_REDIRECT_PAGE = "/pages/task/task_plan?faces-redirect=true";


    @Resource(name = "messageManager")
    private transient MessageManager messageManager;

    private String username;
    private String password;

    public String login() {
        if ("qwe".equals(username) && "123".equals(password)) {
            getSession().setAttribute("user", username);
            return LOGIN_REDIRECT_PAGE;
        } else {
            messageManager.addErrorMessage(BUNDLE_NAME, "authentication_failed");
            return null;
        }
    }

    public String logout() {
        getSession().invalidate();
        return "/pages/authentication?faces-redirect=true";
    }

    private HttpSession getSession() {
        return (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false);
    }

    // getters & setters

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
