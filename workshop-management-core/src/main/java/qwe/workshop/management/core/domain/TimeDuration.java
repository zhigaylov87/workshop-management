package qwe.workshop.management.core.domain;

import java.io.Serializable;
import java.util.concurrent.TimeUnit;

/**
 * User: artem
 * Date: 18.07.15
 * Time: 6:24
 */
public class TimeDuration implements Serializable {

    public static final long ZERO = 0L;

    protected long days;
    protected long hours;
    protected long minutes;
    protected long seconds;
    protected long millis;

    public TimeDuration(long totalMillis) {
        days = TimeUnit.MILLISECONDS.toDays(totalMillis);
        totalMillis -= TimeUnit.DAYS.toMillis(days);

        hours = TimeUnit.MILLISECONDS.toHours(totalMillis);
        totalMillis -= TimeUnit.HOURS.toMillis(hours);

        minutes = TimeUnit.MILLISECONDS.toMinutes(totalMillis);
        totalMillis -= TimeUnit.MINUTES.toMillis(minutes);

        seconds = TimeUnit.MILLISECONDS.toSeconds(totalMillis);
        totalMillis -= TimeUnit.SECONDS.toMillis(seconds);

        millis = totalMillis;
    }

    public long getDays() {
        return days;
    }

    public long getHours() {
        return hours;
    }

    public long getMinutes() {
        return minutes;
    }

    public long getSeconds() {
        return seconds;
    }

    public long getMillis() {
        return millis;
    }

    public long getTotalInMillis() {
        return TimeUnit.DAYS.toMillis(days) +
                TimeUnit.HOURS.toMillis(hours) +
                TimeUnit.MINUTES.toMillis(minutes) +
                TimeUnit.SECONDS.toMillis(seconds) +
                millis;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof TimeDuration)) return false;

        TimeDuration that = (TimeDuration) o;

        if (days != that.days) return false;
        if (hours != that.hours) return false;
        if (minutes != that.minutes) return false;
        if (seconds != that.seconds) return false;
        if (millis != that.millis) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = (int) (days ^ (days >>> 32));
        result = 31 * result + (int) (hours ^ (hours >>> 32));
        result = 31 * result + (int) (minutes ^ (minutes >>> 32));
        result = 31 * result + (int) (seconds ^ (seconds >>> 32));
        result = 31 * result + (int) (millis ^ (millis >>> 32));
        return result;
    }

    @Override
    public String toString() {
        return "TimeDuration {" +
                "days: " + days +
                ", hours: " + hours +
                ", minutes: " + minutes +
                ", seconds: " + seconds +
                ", millis: " + millis +
                '}';
    }
}
