package qwe.workshop.management.web.converter;

import org.apache.commons.lang3.time.FastDateFormat;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import java.util.Date;

/**
 * User: artem
 * Date: 02.07.15
 * Time: 6:19
 */
@FacesConverter("DateTimeConverter")
public class DateTimeConverter implements Converter {

    private static final FastDateFormat DATE_TIME_FORMAT = FastDateFormat.getInstance("dd.MM.yyyy HH:mm");

    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String value) {
        throw new UnsupportedOperationException("getAsObject direction not supported");
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value) {
        if (!(value instanceof Date)) {
            throw new IllegalArgumentException("Value must be instance of " + Date.class + ". Value: " + value);
        }
        return DATE_TIME_FORMAT.format((Date) value);
    }
}
