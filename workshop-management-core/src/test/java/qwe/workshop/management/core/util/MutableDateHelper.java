package qwe.workshop.management.core.util;

import java.util.Date;

/**
 * User: artem
 * Date: 11.07.15
 * Time: 4:17
 */
public class MutableDateHelper implements DateHelper {

    private Date date = new Date();

    @Override
    public Date getCurrentDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
}
