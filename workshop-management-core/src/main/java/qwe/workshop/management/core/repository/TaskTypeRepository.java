package qwe.workshop.management.core.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import qwe.workshop.management.core.domain.task.TaskType;

import java.util.List;

/**
 * User: artem
 * Date: 19.06.15
 * Time: 5:29
 */
public interface TaskTypeRepository extends CrudRepository<TaskType, Long> {

    @Query("select t from TaskType t order by t.name asc")
    public List<TaskType> findAll();

}
