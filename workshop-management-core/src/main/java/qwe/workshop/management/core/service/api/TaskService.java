package qwe.workshop.management.core.service.api;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import qwe.workshop.management.core.domain.task.Task;
import qwe.workshop.management.core.domain.task.TaskStatus;

import javax.validation.Valid;
import java.util.List;

/**
 * User: artem
 * Date: 20.06.15
 * Time: 4:21
 */
public interface TaskService {

    /**
     * @return current tasks - tasks with status not equal to {@link qwe.workshop.management.core.domain.task.TaskStatus#FINISHED}
     */
    public List<Task> findCurrent();

    /**
     * @param pageable pagination information
     * @return page with finished tasks - tasks with status {@link qwe.workshop.management.core.domain.task.TaskStatus#FINISHED}
     */
    public Page<Task> findFinished(Pageable pageable);

    public Task save(@Valid Task task);

    public boolean canBeDelete(TaskStatus taskStatus);

    public void delete(long taskId);

    public boolean canChangeStatus(TaskStatus currentStatus, TaskStatus newStatus);

    public void changeStatus(long taskId, TaskStatus newStatus);
}
