package qwe.workshop.management.core.report.operation;

import com.google.common.primitives.Ints;
import qwe.workshop.management.core.domain.operation.OperationCategory;

import java.io.Serializable;

/**
 * User: artem
 * Date: 05.07.15
 * Time: 19:17
 */
public class OperationByCategoryReportItem implements Serializable {

    private OperationCategory category;
    private int amount;

    public OperationByCategoryReportItem(OperationCategory category, long amount) {
        this.category = category;
        this.amount = Ints.checkedCast(amount);
    }

    public OperationCategory getCategory() {
        return category;
    }

    public int getAmount() {
        return amount;
    }

    @Override
    public String toString() {
        return "OperationByCategoryReportItem {" +
                "category: " + category +
                ", amount: " + amount +
                '}';
    }
}
