package qwe.workshop.management.core.repository;

import org.apache.commons.lang3.tuple.Pair;
import org.joda.time.YearMonth;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import qwe.workshop.management.core.domain.operation.Operation;
import qwe.workshop.management.core.domain.operation.OperationType;
import qwe.workshop.management.core.report.operation.OperationByCategoryReportItem;
import qwe.workshop.management.core.util.NumberUtils;

import java.math.BigInteger;
import java.util.Date;
import java.util.List;
import java.util.Set;

/**
 * User: artem
 * Date: 05.07.15
 * Time: 18:39
 */
public interface OperationReportRepository extends CrudRepository<Operation, Long> {

    @Query("select new org.apache.commons.lang3.tuple.ImmutablePair(op.category.type, sum(op.amount)) " +
           "from Operation op " +
           "where op.date between :startDate and :endDate " +
           "group by op.category.type " +
           "order by op.category.type asc")
    public List<Pair<OperationType, Long>> byType(@Param("startDate") Date startDate, @Param("endDate") Date endDate);

    // TODO: Возможно стоит сделать "join fetch op.category " - посмотреть на запросы
    @Query("select new qwe.workshop.management.core.report.operation.OperationByCategoryReportItem(op.category, sum(op.amount)) " +
            "from Operation op " +
            "where op.category.type = :operationType and op.date between :startDate and :endDate " +
            "group by op.category " +
            "order by sum(op.amount) desc")
    public List<OperationByCategoryReportItem> byCategory(@Param("operationType") OperationType operationType,
                                                          @Param("startDate") Date startDate,
                                                          @Param("endDate") Date endDate);

    /**
     * @param operationYears operationYears
     * @return list of result object arrays sorted by year ascending, month ascending, operation type ascending.
     *         Object array must be using for creation instance of {@link OperationByMonthResultItem}
     */
    @Query(nativeQuery = true,
            value =
                    "select op_year, op_month, op_type, sum(op_amount) from " +

                            "(select op.amount as op_amount, " +
                                    "op_cat.type as op_type, " +
                                    "year(op.operation_date) as op_year, " +
                                    "month(op.operation_date) as op_month " +
                             "from operation op " +
                             "left outer join operation_category op_cat " +
                             "on op.operation_category_id = op_cat.id) ops " +
                    "where op_year in(:operationYears) " +
                    "group by op_year, op_month, op_type " +
                    "order by op_year asc, op_month asc, op_type asc")
    public List<Object[]> byMonth(@Param("operationYears") Set<Integer> operationYears);

    @Query(nativeQuery = true,
            value = "select op.order_source_id, sum(op.amount) as total_amount, count(*) " +
                    "from operation op " +
                    "inner join operation_category op_cat on op_cat.id = op.operation_category_id " +
                    "where op_cat.type = 'INCOME' and op.operation_date between :startDate and :endDate " +
                    "group by op.order_source_id")
    public List<Object[]> byOrderSource(@Param("startDate") Date startDate,
                                                                @Param("endDate") Date endDate);

    @Query(nativeQuery = true,
            value = "select distinct(year(op.operation_date)) as operation_year from operation op order by operation_year")
    public List<Integer> operationYears();

    public static class OperationByMonthResultItem {

        private YearMonth yearMonth;
        private OperationType operationType;
        private int amount;

        public OperationByMonthResultItem(Object[] data) {
            this.yearMonth = new YearMonth((int) data[0], (int) data[1]);
            this.operationType = OperationType.valueOf((String) data[2]);
            this.amount = NumberUtils.toInt((BigInteger) data[3]);
        }

        public YearMonth getYearMonth() {
            return yearMonth;
        }

        public OperationType getOperationType() {
            return operationType;
        }

        public int getAmount() {
            return amount;
        }

        @Override
        public String toString() {
            return "OperationByMonthResultItem {" +
                    "yearMonth: " + yearMonth +
                    ", operationType: " + operationType +
                    ", amount: " + amount +
                    '}';
        }
    }

}
