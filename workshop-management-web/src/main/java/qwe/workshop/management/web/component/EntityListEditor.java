package qwe.workshop.management.web.component;

import qwe.workshop.management.core.domain.Identifiable;

/**
 * User: artem
 * Date: 21.06.15
 * Time: 16:55
 */
public interface EntityListEditor<T extends Identifiable> {

    public void prepareToAdd();

    public void prepareToEdit(T entity);

    public void save();

    public boolean canBeDelete(T entity);

    public void prepareToDelete(T entity);

    public void delete();

    public String getEditorDialogTitle();
}
