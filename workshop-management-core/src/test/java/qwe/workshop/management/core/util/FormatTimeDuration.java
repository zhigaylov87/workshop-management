package qwe.workshop.management.core.util;

/**
 * User: artem
 * Date: 18.07.15
 * Time: 9:01
 */
public class FormatTimeDuration {

    private MutableTimeDuration duration;
    private String formatted;

    public MutableTimeDuration getDuration() {
        return duration;
    }

    public void setDuration(MutableTimeDuration duration) {
        this.duration = duration;
    }

    public String getFormatted() {
        return formatted;
    }

    public void setFormatted(String formatted) {
        this.formatted = formatted;
    }

    @Override
    public String toString() {
        return "FormatTimeDuration {" +
                "duration: " + duration +
                ", formatted: " + FormatUtils.singleQuotes(formatted) +
                '}';
    }
}
