package qwe.workshop.management.web.bean.operation;

import org.primefaces.context.RequestContext;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import qwe.workshop.management.core.domain.operation.OrderSource;
import qwe.workshop.management.core.service.ServiceException;
import qwe.workshop.management.core.service.api.OrderSourceService;
import qwe.workshop.management.web.component.EntityListEditor;
import qwe.workshop.management.web.util.MessageManager;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.faces.context.FacesContext;
import javax.validation.ConstraintViolationException;
import java.io.Serializable;
import java.util.Arrays;
import java.util.List;

/**
 * User: artem
 * Date: 12.07.15
 * Time: 1:30
 */
@Component("orderSourcesBean")
@Scope("view")
public class OrderSourcesBean implements EntityListEditor<OrderSource>, Serializable {

    private static final String BUNDLE_NAME = "qwe.workshop.management.web.i18n.operation.order_sources";

    @Resource(name = "orderSourceService")
    private transient OrderSourceService orderSourceService;

    @Resource(name = "messageManager")
    private transient MessageManager messageManager;

    private List<OrderSource> orderSources;
    private OrderSource preparedOrderSource;
    private String editorDialogTitle;

    @PostConstruct
    private void init() {
        reloadList();
    }

    @Override
    public void prepareToAdd() {
        preparedOrderSource = new OrderSource();
        editorDialogTitle = messageManager.getMessage(BUNDLE_NAME, "add_dialog_title");
    }

    @Override
    public void prepareToEdit(OrderSource entity) {
        preparedOrderSource = new OrderSource(entity);
        editorDialogTitle = messageManager.getMessage(BUNDLE_NAME, "edit_dialog_title");
    }

    @Override
    public void save() {
        try {
            preparedOrderSource = orderSourceService.save(preparedOrderSource);
        } catch (ConstraintViolationException e) {
            messageManager.addErrorMessage(e);
            FacesContext.getCurrentInstance().validationFailed();
            return;
        }

        messageManager.addInfoMessage(BUNDLE_NAME, "save_success", preparedOrderSource.getName());
        reloadList();
        RequestContext.getCurrentInstance().update(Arrays.asList("entityListEditorTableForm", "messages"));
    }

    @Override
    public boolean canBeDelete(OrderSource entity) {
        return true; // TODO: Проверять, что нет опеараций с таким источником заказа, если есть то нельзя удалять
    }

    @Override
    public void prepareToDelete(OrderSource entity) {
        preparedOrderSource = entity;
        editorDialogTitle = messageManager.getMessage(BUNDLE_NAME, "delete_dialog_title", preparedOrderSource.getName());
    }

    @Override
    public void delete() {
        try {
            orderSourceService.delete(preparedOrderSource.getId());
        } catch (ServiceException e) {
            messageManager.addErrorMessage(e);
            return;
        }

        messageManager.addInfoMessage(BUNDLE_NAME, "delete_success", preparedOrderSource.getName());
        reloadList();
    }

    @Override
    public String getEditorDialogTitle() {
        return editorDialogTitle;
    }

    private void reloadList() {
        orderSources = orderSourceService.findAll();
    }

    // setters & getters

    public List<OrderSource> getOrderSources() {
        return orderSources;
    }

    public OrderSource getPreparedOrderSource() {
        return preparedOrderSource;
    }
}
