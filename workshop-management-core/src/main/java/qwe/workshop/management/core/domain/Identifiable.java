package qwe.workshop.management.core.domain;

/**
 * User: artem
 * Date: 01.03.14
 * Time: 14:21
 */
public interface Identifiable {

    /**
     * @return entity id or {@code null} if entity is not saved in database
     */
    public Long getId();

}
