package qwe.workshop.management.core.service.api;

import qwe.workshop.management.core.report.operation.OperationByCategoryReport;
import qwe.workshop.management.core.report.operation.OperationByMonthReport;
import qwe.workshop.management.core.report.operation.OperationByOrderSourceReport;
import qwe.workshop.management.core.report.operation.OperationByTypeReport;
import qwe.workshop.management.core.util.DateInterval;

import java.util.List;
import java.util.Set;

/**
 * User: artem
 * Date: 05.07.15
 * Time: 18:40
 */
public interface OperationReportService {

    public OperationByTypeReport getOperationByTypeReport(DateInterval interval);

    public OperationByCategoryReport getOperationByCategoryReport(DateInterval interval);

    public OperationByMonthReport getOperationByMonthReport(Set<Integer> years);

    public OperationByOrderSourceReport getOperationByOrderSourceReport(DateInterval interval);

    public List<Integer> getOperationYears();
}
