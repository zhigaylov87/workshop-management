package qwe.workshop.management.core.service.api;

import qwe.workshop.management.core.domain.operation.OrderSource;

import javax.validation.Valid;
import java.util.List;

/**
 * User: artem
 * Date: 12.07.15
 * Time: 1:20
 */
public interface OrderSourceService {

    public List<OrderSource> findAll();

    public OrderSource save(@Valid OrderSource orderSource);

    public void delete(long orderSourceId);

}
