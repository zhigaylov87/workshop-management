package qwe.workshop.management.web.util.exceptionhandler;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import qwe.workshop.management.core.CoreException;

import javax.faces.FacesException;
import javax.faces.application.NavigationHandler;
import javax.faces.context.ExceptionHandler;
import javax.faces.context.ExceptionHandlerWrapper;
import javax.faces.context.FacesContext;
import javax.faces.event.ExceptionQueuedEvent;
import javax.faces.event.ExceptionQueuedEventContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Iterator;

/**
 * User: artem
 * Date: 14.02.14
 * Time: 1:27
 */
public class NotAjaxExceptionHandler extends ExceptionHandlerWrapper {

    private static final Logger log = LoggerFactory.getLogger(NotAjaxExceptionHandler.class);
    private static final String ERROR_PAGE = "/pages/error/internal_error.xhtml";

    private ExceptionHandler wrapped;

    public NotAjaxExceptionHandler(ExceptionHandler wrapped) {
        this.wrapped = wrapped;
    }

    @Override
    public ExceptionHandler getWrapped() {
        return wrapped;
    }

    @Override
    public void handle() throws FacesException {
        final Iterator<ExceptionQueuedEvent> i = getUnhandledExceptionQueuedEvents().iterator();

        while (i.hasNext()) {
            ExceptionQueuedEvent event = i.next();
            ExceptionQueuedEventContext context =
                    (ExceptionQueuedEventContext) event.getSource();

            // get the exception from context
            Throwable t = context.getException();

            final FacesContext fc = FacesContext.getCurrentInstance();
//            final Map<String, Object> requestMap = fc.getExternalContext().getRequestMap();
            final NavigationHandler nav = fc.getApplication().getNavigationHandler();

            //here you do what ever you want with exception
            try {
                log.error(NotAjaxExceptionHandler.class.getName() +
                        ": An exception occurred processing JSF not ajax request." +
                        " Error page '" + ERROR_PAGE + "' will be shown.",
                        t);

                HttpServletResponse response = (HttpServletResponse) fc.getExternalContext().getResponse();
                HttpServletRequest request = (HttpServletRequest) fc.getExternalContext().getRequest();

                String errorPage = request.getContextPath() + ERROR_PAGE;
                response.sendRedirect(errorPage);
//                response.getWriter().print(
//                        "<script type=\"text/javascript\">" +
//                            "window.location.href = '" + errorPage + "'" +
//                        "</script>");
//                fc.responseComplete();
//                fc.renderResponse();
            } catch (Exception e) {
                log.error("Error while exception handling", e);
                throw new CoreException(e);
            } finally {
                //remove it from queue
                i.remove();
            }
        }
        //parent hanle
        getWrapped().handle();
    }

}