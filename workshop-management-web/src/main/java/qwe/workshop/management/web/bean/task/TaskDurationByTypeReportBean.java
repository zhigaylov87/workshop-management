package qwe.workshop.management.web.bean.task;

import org.primefaces.model.chart.PieChartModel;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import qwe.workshop.management.core.domain.TimeDuration;
import qwe.workshop.management.core.domain.task.TaskType;
import qwe.workshop.management.core.report.task.TaskDurationByTypeReport;
import qwe.workshop.management.core.service.api.TaskReportService;
import qwe.workshop.management.core.util.DateHelper;
import qwe.workshop.management.core.util.DateInterval;
import qwe.workshop.management.core.util.FormatUtils;
import qwe.workshop.management.web.component.DateIntervalFilter;
import qwe.workshop.management.web.util.Colors;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import java.io.Serializable;
import java.util.List;

/**
 * User: artem
 * Date: 11.07.15
 * Time: 6:15
 */
@Component("taskDurationByTypeReportBean")
@Scope("view")
public class TaskDurationByTypeReportBean implements DateIntervalFilter, Serializable {

    @Resource(name = "taskReportService")
    private transient TaskReportService taskReportService;

    @Resource(name = "dateHelper")
    private transient DateHelper dateHelper;

    private DateInterval dateInterval;
    private TaskDurationByTypeReport report;
    private PieChartModel pieChartModel;

    @PostConstruct
    private void init() {
        dateInterval = new DateInterval(dateHelper.getCurrentDate());
        reloadReport();
    }

    @Override
    public void filteringInvoked() {
        reloadReport();
    }

    @Override
    public void resetInvoked() {
        throw new UnsupportedOperationException("Reset not supported");
    }

    private void reloadReport() {
        report = taskReportService.getDurationByTypeReport(dateInterval);
        if (pieChartModel == null) {
            pieChartModel = new PieChartModel();
            pieChartModel.setLegendPosition("w");
            pieChartModel.setShowDataLabels(true);
            pieChartModel.setDiameter(500);
        } else {
            pieChartModel.clear();
        }

        List<TaskType> types = report.getTypes();
        pieChartModel.setSeriesColors(Colors.getSeriesColors(types.size()));
        for (TaskType type : types) {
            TimeDuration duration = report.getDuration(type);
            pieChartModel.set(type.getName() + ": " + FormatUtils.formatDuration(duration), duration.getTotalInMillis());
        }
    }

    // setter & getters

    @Override
    public DateInterval getDateInterval() {
        return dateInterval;
    }

    public PieChartModel getPieChartModel() {
        return pieChartModel;
    }
}
