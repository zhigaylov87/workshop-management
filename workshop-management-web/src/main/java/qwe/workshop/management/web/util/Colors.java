package qwe.workshop.management.web.util;

import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * User: artem
 * Date: 14.07.15
 * Time: 0:14
 */
public class Colors {

    private static final List<String> COLORS = Arrays.asList(
            "67CF56", "FFB832", "FEF761", "75CDF3", "FF6C6A",
            "DEEC94", "E485C9", "617AF0", "ECC951", "FF7701",
            "B6CF57", "DD0400");

    private static final int NEXT_COLOR_INDEX_SHIFT = 3;

    public static List<String> get(int count) {
        if (count <= 0) {
            return Collections.emptyList();
        }
        List<String> colors;
        if (count <= COLORS.size()) {
            colors = COLORS.subList(0, count);
        } else {
            colors = new ArrayList<>(count);
            colors.addAll(COLORS);
            int lastColorIndex = COLORS.size() - 1;
            while (colors.size() < count) {
                lastColorIndex += NEXT_COLOR_INDEX_SHIFT;
                if (lastColorIndex > COLORS.size() - 1) {
                    lastColorIndex -= COLORS.size();
                }
                colors.add(COLORS.get(lastColorIndex));
            }
        }

        return Collections.unmodifiableList(colors);
    }

    /**
     * @param count colors count
     * @return colors hex without "<b>#</b>", divided by ",". For example: "<b>67CF56,FFB832</b>"
     */
    public static String getSeriesColors(int count) {
        return StringUtils.join(get(count), ",");
    }

}
