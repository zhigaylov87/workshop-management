package qwe.workshop.management.core.util;

import qwe.workshop.management.core.domain.TimeDuration;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * User: artem
 * Date: 02.09.14
 * Time: 1:00
 */
public class FormatUtils {

    private static final int AMOUNT_GROUP_LENGTH = 3;

    /**
     * Format amount by grouping by three numbers with space as separator.<br/>
     * For expample:<br/>
     * <ul>
     * <li>12</li>
     * <li>123</li>
     * <li>1 234</li>
     * <li>12 345</li>
     * <li>123 456</li>
     * <li>1 234 567</li>
     * </ul>
     * @param amount amount for formatting
     * @return formatted amount
     */
    public static String formatAmount(int amount) {
        String amountString = Integer.toString(amount);
        String formattingAmount = "";

        int currentGroupLength = 0;
        for (int i = amountString.length() - 1; i >= 0; i--) {
            if (currentGroupLength < AMOUNT_GROUP_LENGTH) {
                formattingAmount = amountString.charAt(i) + formattingAmount;
                currentGroupLength++;
            } else {
                formattingAmount = amountString.charAt(i) + " " + formattingAmount;
                currentGroupLength = 1;
            }
        }

        return formattingAmount;
    }

    public static String formatDate(Date date) {
        return getDateFormat().format(date);
    }

    public static DateFormat getDateFormat() {
        return new SimpleDateFormat("dd.MM.yyyy");
    }

    /**
     * @param s string for quoting
     * @return {@code null} if string is {@code null} or single quoting string
     */
    public static String singleQuotes(String s) {
        return s == null ? null : "'" + s + "'";
    }

    // TODO: Вынести строки в локализацию
    public static String formatDuration(TimeDuration duration) {
        StringBuilder s = new StringBuilder();

        boolean previousLargestPrinted = false;
        if (duration.getDays() != TimeDuration.ZERO) {
            s.append(duration.getDays()).append(" д.");
            previousLargestPrinted = true;
        }
        if (previousLargestPrinted || duration.getHours() != TimeDuration.ZERO) {
            s.append(previousLargestPrinted ? " " : "").append(duration.getHours()).append(" ч.");
            previousLargestPrinted = true;
        }
        if (previousLargestPrinted || duration.getMinutes() != TimeDuration.ZERO) {
            s.append(previousLargestPrinted ? " " : "").append(duration.getMinutes()).append(" мин.");
            previousLargestPrinted = true;
        }
        if (previousLargestPrinted || duration.getSeconds() != TimeDuration.ZERO) {
            s.append(previousLargestPrinted ? " " : "").append(duration.getSeconds()).append(" сек.");
        }

        return s.toString();
    }
}
