package qwe.workshop.management.web.util;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * User: artem
 * Date: 20.05.15
 * Time: 2:42
 */
@WebFilter(filterName = "AuthenticationFilter", urlPatterns = {"*.xhtml"})
public class AuthenticationFilter implements Filter {

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        // do nothing
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest req = (HttpServletRequest) request;
        HttpServletResponse res = (HttpServletResponse) response;
        HttpSession session = req.getSession(false);

        String reqURI = req.getRequestURI();
        if (reqURI.endsWith("authentication.xhtml") ||
                (session != null && session.getAttribute("user") != null) ||
                reqURI.contains("javax.faces.resource")) {
            chain.doFilter(request, response);
        } else {
            res.sendRedirect(req.getContextPath() + "/pages/authentication.xhtml");
        }
    }

    @Override
    public void destroy() {
        // do nothing
    }
}
