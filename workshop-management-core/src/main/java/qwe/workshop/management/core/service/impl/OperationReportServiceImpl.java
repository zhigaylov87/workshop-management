package qwe.workshop.management.core.service.impl;

import com.google.common.primitives.Ints;
import org.apache.commons.lang3.tuple.Pair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import qwe.workshop.management.core.CoreException;
import qwe.workshop.management.core.domain.operation.OperationType;
import qwe.workshop.management.core.domain.operation.OrderSource;
import qwe.workshop.management.core.report.operation.*;
import qwe.workshop.management.core.repository.OperationReportRepository;
import qwe.workshop.management.core.repository.OrderSourceRepository;
import qwe.workshop.management.core.service.api.OperationReportService;
import qwe.workshop.management.core.util.DateInterval;
import qwe.workshop.management.core.util.IdentifiableList;
import qwe.workshop.management.core.util.NumberUtils;

import javax.annotation.Resource;
import java.math.BigInteger;
import java.util.*;

import static qwe.workshop.management.core.repository.OperationReportRepository.OperationByMonthResultItem;

/**
 * User: artem
 * Date: 05.07.15
 * Time: 19:32
 */
@Service("operationReportService")
@Repository
@Transactional(readOnly = true)
public class OperationReportServiceImpl implements OperationReportService {

    private static final Logger log = LoggerFactory.getLogger(OperationReportServiceImpl.class);

    @Autowired
    private OperationReportRepository operationReportRepository;

    @Resource(name = "orderSourceRepository")
    private OrderSourceRepository orderSourceRepository;

    @Override
    public OperationByTypeReport getOperationByTypeReport(DateInterval interval) {
        String logMsg = " of fetching operation by type report. Interval: " + interval;
        try {
            log.info("Start" + logMsg);
            List<Pair<OperationType, Long>> typeToAmount = operationReportRepository.byType(
                    interval.getStartDate(), interval.getEndDate());

            OperationReportAmounts reportAmounts;
            switch (typeToAmount.size()) {
                case 0:
                    reportAmounts = new OperationReportAmounts(0, 0);
                    break;
                case 1:
                    Pair<OperationType, Long> item = typeToAmount.get(0);
                    int amount = Ints.checkedCast(item.getRight());
                    reportAmounts = new OperationReportAmounts(
                            item.getLeft() == OperationType.EXPENSE ? amount : 0,
                            item.getLeft() == OperationType.INCOME ? amount : 0);
                    break;
                case 2:
                    Pair<OperationType, Long> expenseItem = typeToAmount.get(0);
                    Pair<OperationType, Long> incomeItem = typeToAmount.get(1);
                    if (expenseItem.getKey() != OperationType.EXPENSE) {
                        throw new IllegalArgumentException("Illegal expense item: " + expenseItem);
                    }
                    if (incomeItem.getKey() != OperationType.INCOME) {
                        throw new IllegalArgumentException("Illegal income item: " + expenseItem);
                    }
                    reportAmounts = new OperationReportAmounts(
                            Ints.checkedCast(expenseItem.getRight()),
                            Ints.checkedCast(incomeItem.getRight()));
                    break;
                default:
                    throw new CoreException("Illegal operation type to amount result: " +
                            Arrays.toString(typeToAmount.toArray()));
            }

            OperationByTypeReport report = new OperationByTypeReport(reportAmounts);
            log.info("End" + logMsg + ". Report: " + report);
            return report;
        } catch (Exception e) {
            log.error("Error" + logMsg, e);
            throw new CoreException(e);
        }
    }

    @Override
    public OperationByCategoryReport getOperationByCategoryReport(DateInterval interval) {
        String logMsg = " of fetching operation by category report. Interval: " + interval;
        try {
            log.info("Start" + logMsg);
            OperationByCategoryReport report = new OperationByCategoryReport(
                    operationReportRepository.byCategory(OperationType.EXPENSE, interval.getStartDate(), interval.getEndDate()),
                    operationReportRepository.byCategory(OperationType.INCOME, interval.getStartDate(), interval.getEndDate()));
            log.info("End" + logMsg + ". Report: " + report);
            return report;
        } catch (Exception e) {
            log.error("Error" + logMsg, e);
            throw new CoreException(e);
        }
    }

    @Override
    public OperationByMonthReport getOperationByMonthReport(Set<Integer> years) {
        List<Integer> yearsList = new ArrayList<>(years);
        Collections.sort(yearsList);
        String logMsg = " of fetching operation by month report. Years: " + Arrays.toString(yearsList.toArray());
        try {
            log.info("Start" + logMsg);

            OperationByMonthReport report = new OperationByMonthReport();
            OperationByMonthResultItem previousResultItem = null;
            for (Object[] resultItemArray : operationReportRepository.byMonth(years)) {
                if (log.isDebugEnabled()) {
                    log.debug("Result item array: " + Arrays.toString(resultItemArray));
                }
                OperationByMonthResultItem currentResultItem = new OperationByMonthResultItem(resultItemArray);
                if (log.isDebugEnabled()) {
                    log.debug("Result item: " + currentResultItem);
                }

                if (previousResultItem == null) {
                    previousResultItem = currentResultItem;
                } else if (previousResultItem.getYearMonth().equals(currentResultItem.getYearMonth())) {
                    report.add(currentResultItem.getYearMonth(), createOperationReportAmounts(previousResultItem, currentResultItem));
                    previousResultItem = null;
                } else {
                    report.add(previousResultItem.getYearMonth(), createOperationReportAmounts(previousResultItem));
                    previousResultItem = currentResultItem;
                }
            }

            log.info("End" + logMsg + ". Report: " + report);
            return report;
        } catch (Exception e) {
            log.error("Error" + logMsg, e);
            throw new CoreException(e);
        }
    }

    @Override
    public OperationByOrderSourceReport getOperationByOrderSourceReport(DateInterval interval) {
        String logMsg = " of fetching operation by order source report. Interval: " + interval;
        try {
            log.info("Start" + logMsg);

            IdentifiableList<OrderSource> orderSources = new IdentifiableList<>(orderSourceRepository.findAll());
            List<OperationByOrderSourceReportItem> reportItems = new ArrayList<>();

            for (Object[] resultItem : operationReportRepository.byOrderSource(interval.getStartDate(), interval.getEndDate())) {
                if (log.isDebugEnabled()) {
                    log.debug("Result item array: " + Arrays.toString(resultItem));
                }
                BigInteger orderSourceBigInteger = (BigInteger) resultItem[0];
                Long orderSourceId = orderSourceBigInteger == null ? null : NumberUtils.toLong(orderSourceBigInteger);
                int totalIncome = NumberUtils.toInt((BigInteger) resultItem[1]);
                int operationsCount = NumberUtils.toInt((BigInteger) resultItem[2]);

                reportItems.add(new OperationByOrderSourceReportItem(
                        orderSourceId == null ? null : orderSources.getById(orderSourceId),
                        totalIncome, operationsCount));
            }

            OperationByOrderSourceReport report = new OperationByOrderSourceReport(reportItems);
            log.info("End" + logMsg + ". Report: " + report);
            return report;
        } catch (Exception e) {
            log.error("Error" + logMsg, e);
            throw new CoreException(e);
        }
    }

    @Override
    public List<Integer> getOperationYears() {
        String logMsg = " of fetching operation years";
        try {
            log.info("Start" + logMsg);
            List<Integer> years = operationReportRepository.operationYears();
            log.info("End" + logMsg + ". Years: " + Arrays.toString(years.toArray()));
            return years;
        } catch (Exception e) {
            log.error("Error" + logMsg, e);
            throw new CoreException(e);
        }
    }

    private OperationReportAmounts createOperationReportAmounts(OperationByMonthResultItem expenseItem,
                                                                OperationByMonthResultItem incomeItem) {
        if (expenseItem.getOperationType() != OperationType.EXPENSE) {
            throw new IllegalArgumentException("Illegal expense item: " + expenseItem);
        }
        if (incomeItem.getOperationType() != OperationType.INCOME) {
            throw new IllegalArgumentException("Illegal income item: " + incomeItem);
        }
        return new OperationReportAmounts(expenseItem.getAmount(), incomeItem.getAmount());
    }

    private OperationReportAmounts createOperationReportAmounts(OperationByMonthResultItem item) {
        int expense = item.getOperationType() == OperationType.EXPENSE ? item.getAmount() : 0;
        int income = item.getOperationType() == OperationType.INCOME ? item.getAmount() : 0;
        return new OperationReportAmounts(expense, income);
    }
}
