package qwe.workshop.management.core.repository;

import org.springframework.data.repository.CrudRepository;
import qwe.workshop.management.core.domain.task.TaskInterval;

/**
 * User: artem
 * Date: 25.06.15
 * Time: 2:50
 */
public interface TaskIntervalRepository extends CrudRepository<TaskInterval, Long> {
}
