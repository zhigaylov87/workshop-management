package qwe.workshop.management.core.domain.operation;

import org.hibernate.validator.constraints.NotEmpty;
import qwe.workshop.management.core.domain.Identifiable;
import qwe.workshop.management.core.util.FormatUtils;
import qwe.workshop.management.core.util.ObjectUtils;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.io.Serializable;

/**
 * User: artem
 * Date: 12.07.15
 * Time: 0:53
 */
@Entity
@Table(name = "ORDER_SOURCE", uniqueConstraints = {@UniqueConstraint(columnNames = "NAME")})
public class OrderSource implements Serializable, Identifiable {

    private Long id;
    private String name;

    public OrderSource() {
    }

    public OrderSource(OrderSource other) {
        this.id = other.id;
        this.name = other.name;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @NotEmpty(message = "{OrderSource.name.NotEmpty}")
    @Size(max = 128, message = "{OrderSource.name.Size}")
    @Column(name = "NAME", unique = true, nullable = false)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof OrderSource)) return false;

        OrderSource that = (OrderSource) o;
        return ObjectUtils.equalsIds(this, that);
    }

    @Override
    public int hashCode() {
        return id != null ? id.hashCode() : 0;
    }

    @Override
    public String toString() {
        return "OrderSource {" +
                "id: " + id +
                ", name: " + FormatUtils.singleQuotes(name) +
                '}';
    }
}
