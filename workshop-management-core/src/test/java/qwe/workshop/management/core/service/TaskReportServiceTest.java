package qwe.workshop.management.core.service;

import org.junit.Test;
import qwe.workshop.management.core.domain.task.Task;
import qwe.workshop.management.core.domain.task.TaskStatus;
import qwe.workshop.management.core.domain.task.TaskType;
import qwe.workshop.management.core.report.task.TaskDurationByTypeReport;
import qwe.workshop.management.core.service.api.TaskReportService;
import qwe.workshop.management.core.service.api.TaskService;
import qwe.workshop.management.core.service.api.TaskTypeService;
import qwe.workshop.management.core.util.DateInterval;

import javax.annotation.Resource;
import java.util.Date;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

/**
 * TODO: вынести тестовые наборы в ресурсы
 * User: artem
 * Date: 11.07.15
 * Time: 4:11
 */
public class TaskReportServiceTest extends AbstractServiceTest {

    @Resource(name = "taskTypeService")
    private TaskTypeService taskTypeService;
    @Resource(name = "taskService")
    private TaskService taskService;
    @Resource(name = "taskReportService")
    private TaskReportService taskReportService;

    @Test
    public void test1() {
        TaskType type = newSavedFooTaskType();
        Task task = newSavedFooTask(type);

        Date taskStart = setCurrentDate("2015-01-01 08:15:42");
        taskService.changeStatus(task.getId(), TaskStatus.ACTIVE);
        Date taskEnd = setCurrentDate("2015-01-01 17:42:37");
        taskService.changeStatus(task.getId(), TaskStatus.PAUSED);

        // Both task interval inside report inverval. Additional implicit check for report date time cutting.
        DateInterval bothInsideInterval = newDateInterval("2015-01-01 20:34:00", "2015-01-02 23:56:00");
        TaskDurationByTypeReport report = taskReportService.getDurationByTypeReport(bothInsideInterval);
        assertEquals(taskEnd.getTime() - taskStart.getTime(), report.getDuration(type).getTotalInMillis());
    }

    @Test
    public void test2() {
        TaskType type = newSavedFooTaskType();
        Task task = newSavedFooTask(type);

        setCurrentDate("2015-01-01 22:15:42");
        taskService.changeStatus(task.getId(), TaskStatus.ACTIVE);
        Date taskEnd = setCurrentDate("2015-01-02 02:42:37");
        taskService.changeStatus(task.getId(), TaskStatus.PAUSED);

        // Task start outside report interval. Additional implicit check for report date time cutting.
        DateInterval startOutsideInterval = newDateInterval("2015-01-02 09:00:00", "2015-01-02 22:00:00");
        TaskDurationByTypeReport report = taskReportService.getDurationByTypeReport(startOutsideInterval);
        assertEquals(taskEnd.getTime() - startOutsideInterval.getStartDateBottom().getTime(), report.getDuration(type).getTotalInMillis());
    }

    @Test
    public void test3() {
        TaskType type = newSavedFooTaskType();
        Task task = newSavedFooTask(type);

        Date taskStart = setCurrentDate("2015-01-01 22:15:42");
        taskService.changeStatus(task.getId(), TaskStatus.ACTIVE);
        setCurrentDate("2015-01-02 02:42:37");
        taskService.changeStatus(task.getId(), TaskStatus.PAUSED);

        // Task end outside report interval. Additional implicit check for report date time cutting.
        DateInterval endOutsideInterval = newDateInterval("2015-01-01 09:00:00", "2015-01-01 14:15:42");
        TaskDurationByTypeReport report = taskReportService.getDurationByTypeReport(endOutsideInterval);
        assertEquals(endOutsideInterval.getEndDateTop().getTime() - taskStart.getTime(), report.getDuration(type).getTotalInMillis());
    }

    @Test
    public void test4() {
        TaskType type = newSavedFooTaskType();
        Task task = newSavedFooTask(type);

        setCurrentDate("2015-01-01 08:15:42");
        taskService.changeStatus(task.getId(), TaskStatus.ACTIVE);
        setCurrentDate("2015-01-03 17:42:37");
        taskService.changeStatus(task.getId(), TaskStatus.PAUSED);

        // Report interval inside task interval
        DateInterval interval = newDateInterval("2015-01-02 12:34:00", "2015-01-02 22:00:00");
        TaskDurationByTypeReport report = taskReportService.getDurationByTypeReport(interval);
        assertEquals(interval.getEndDateTop().getTime() - interval.getStartDateBottom().getTime(),
                report.getDuration(type).getTotalInMillis());
    }

    @Test
    public void test5() {
        TaskType type = newSavedFooTaskType();
        Task task = newSavedFooTask(type);

        setCurrentDate("2015-01-01 08:15:42");
        taskService.changeStatus(task.getId(), TaskStatus.ACTIVE);
        setCurrentDate("2015-01-01 17:42:37");
        taskService.changeStatus(task.getId(), TaskStatus.PAUSED);

        // Task interval fully early than report interval
        DateInterval interval = newDateInterval("2015-01-02 12:34:00", "2015-01-02 22:00:00");
        TaskDurationByTypeReport report = taskReportService.getDurationByTypeReport(interval);
        assertFalse(report.getTypes().contains(type));
    }

    @Test
    public void test6() {
        TaskType type = newSavedFooTaskType();
        Task task = newSavedFooTask(type);

        setCurrentDate("2015-01-02 08:15:42");
        taskService.changeStatus(task.getId(), TaskStatus.ACTIVE);
        setCurrentDate("2015-01-02 17:42:37");
        taskService.changeStatus(task.getId(), TaskStatus.PAUSED);

        // Task interval fully greater than report interval
        DateInterval interval = newDateInterval("2015-01-01 12:34:00", "2015-01-01 22:00:00");
        TaskDurationByTypeReport report = taskReportService.getDurationByTypeReport(interval);
        assertFalse(report.getTypes().contains(type));
    }

}
