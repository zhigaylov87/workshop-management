package qwe.workshop.management.core.util;

import org.springframework.stereotype.Component;

import java.util.Date;

/**
 * User: artem
 * Date: 25.06.15
 * Time: 3:08
 */
@Component("dateHelper")
public class DefaultDateHelper implements DateHelper {

    public Date getCurrentDate() {
        return new Date();
    }

}
