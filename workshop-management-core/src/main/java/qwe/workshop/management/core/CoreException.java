package qwe.workshop.management.core;

/**
 * User: artem
 * Date: 27.12.14
 * Time: 4:26
 */
public class CoreException extends RuntimeException {

    public CoreException(String message) {
        super(message);
    }

    public CoreException(String message, Throwable cause) {
        super(message, cause);
    }

    public CoreException(Throwable cause) {
        super(cause);
    }
}
