package qwe.workshop.management.core.report.operation;

import qwe.workshop.management.core.domain.operation.OrderSource;

import java.io.Serializable;

/**
 * User: artem
 * Date: 12.07.15
 * Time: 2:56
 */
public class OperationByOrderSourceReportItem implements Serializable {

    private OrderSource orderSource;
    private int totalIncome;
    private int operationsCount;

    /**
     * @param orderSource order source or {@code null} for operations with not defined order source
     * @param totalIncome total income
     * @param operationsCount operations count
     */
    public OperationByOrderSourceReportItem(OrderSource orderSource, int totalIncome, int operationsCount) {
        this.orderSource = orderSource;
        this.totalIncome = totalIncome;
        this.operationsCount = operationsCount;
    }

    public OrderSource getOrderSource() {
        return orderSource;
    }

    public int getTotalIncome() {
        return totalIncome;
    }

    public int getOperationsCount() {
        return operationsCount;
    }

    @Override
    public String toString() {
        return "OperationByOrderSourceReportItem {" +
                "orderSource: " + orderSource +
                ", totalIncome: " + totalIncome +
                ", operationsCount: " + operationsCount +
                '}';
    }
}
