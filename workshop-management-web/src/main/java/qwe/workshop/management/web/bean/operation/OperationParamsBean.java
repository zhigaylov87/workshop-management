package qwe.workshop.management.web.bean.operation;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import qwe.workshop.management.core.util.WebUtils;

import javax.annotation.PostConstruct;
import java.io.Serializable;
import java.util.Arrays;
import java.util.List;

/**
 * User: artem
 * Date: 12.07.15
 * Time: 1:31
 */
@Component("operationParamsBean")
@Scope("view")
public class OperationParamsBean implements Serializable {

    private static final int OPERATION_CATEGORY_MODE_INDEX = 0;
    private static final int ORDER_SOURCE_MODE_INDEX = 1;
    private static final List<Integer> MODES = Arrays.asList(OPERATION_CATEGORY_MODE_INDEX, ORDER_SOURCE_MODE_INDEX);

    private int modeIndex;

    @PostConstruct
    private void init() {
        Integer modeParam = WebUtils.getIntegerRequestParameter("mode");
        modeIndex = modeParam == null ? OPERATION_CATEGORY_MODE_INDEX : modeParam;
        if (!MODES.contains(modeIndex)) {
            throw new IllegalArgumentException("Illegal mode index: " + modeIndex);
        }
    }

    // getters

    public int getOperationCategoryModeIndex() {
        return OPERATION_CATEGORY_MODE_INDEX;
    }

    public int getOrderSourceModeIndex() {
        return ORDER_SOURCE_MODE_INDEX;
    }

    public boolean isOperationCategoryMode() {
        return modeIndex == OPERATION_CATEGORY_MODE_INDEX;
    }

    public boolean isOrderSourceMode() {
        return modeIndex == ORDER_SOURCE_MODE_INDEX;
    }
}
