package qwe.workshop.management.web.bean.task;

import org.springframework.context.annotation.Scope;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;
import qwe.workshop.management.core.domain.task.Task;
import qwe.workshop.management.core.service.api.TaskService;
import qwe.workshop.management.web.component.table.PaginationDataModel;

import javax.annotation.Resource;
import java.io.Serializable;

/**
 * User: artem
 * Date: 22.06.15
 * Time: 1:31
 */
@Component("taskPlanFinishedBean")
@Scope("view")
public class TaskPlanFinishedBean implements PaginationDataModel.Handler<Task>, Serializable {

    @Resource(name = "taskService")
    private transient TaskService taskService;

    private PaginationDataModel<Task> finishedTasksModel = new PaginationDataModel<>(this);

    @Override
    public String getDataTableId() {
        return ":finishedTasksForm:finishedTasksTable";
    }

    @Override
    public Page<Task> loadPage(Pageable pageable) {
        return taskService.findFinished(pageable);
    }

    @Override
    public void deselect() {
        // Do nothing. No selection in table.
    }

    // setters & getters


    public PaginationDataModel<Task> getFinishedTasksModel() {
        return finishedTasksModel;
    }
}
