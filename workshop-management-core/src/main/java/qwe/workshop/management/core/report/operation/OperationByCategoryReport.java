package qwe.workshop.management.core.report.operation;

import java.io.Serializable;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * User: artem
 * Date: 05.07.15
 * Time: 19:17
 */
public class OperationByCategoryReport implements Serializable {

    private List<OperationByCategoryReportItem> expenseItems;
    private List<OperationByCategoryReportItem> incomeItems;

    public OperationByCategoryReport(List<OperationByCategoryReportItem> expenseItems,
                                     List<OperationByCategoryReportItem> incomeItems) {
        this.expenseItems = Collections.unmodifiableList(expenseItems);
        this.incomeItems = Collections.unmodifiableList(incomeItems);
    }

    public List<OperationByCategoryReportItem> getExpenseItems() {
        return expenseItems;
    }

    public List<OperationByCategoryReportItem> getIncomeItems() {
        return incomeItems;
    }

    @Override
    public String toString() {
        return "OperationByCategoryReport {" +
                "expenseItems: " + Arrays.toString(expenseItems.toArray(new OperationByCategoryReportItem[expenseItems.size()])) +
                ", incomeItems: " + Arrays.toString(incomeItems.toArray(new OperationByCategoryReportItem[incomeItems.size()])) +
                '}';
    }
}
