package qwe.workshop.management.core.service.api;

import qwe.workshop.management.core.domain.task.TaskType;

import javax.validation.Valid;
import java.util.List;

/**
 * User: artem
 * Date: 20.06.15
 * Time: 4:21
 */
public interface TaskTypeService {

    public List<TaskType> findAll();

    public TaskType save(@Valid TaskType taskType);

    public void delete(long taskTypeId);

}
