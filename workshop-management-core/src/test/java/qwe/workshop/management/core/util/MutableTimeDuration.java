package qwe.workshop.management.core.util;

import qwe.workshop.management.core.domain.TimeDuration;

/**
 * User: artem
 * Date: 18.07.15
 * Time: 6:50
 */
public class MutableTimeDuration extends TimeDuration {

    public MutableTimeDuration() {
        super(ZERO);
    }

    public MutableTimeDuration(long days, long hours, long minutes, long seconds) {
        super(ZERO);
        setDays(days);
        setHours(hours);
        setMinutes(minutes);
        setSeconds(seconds);
    }

    public void setDays(long days) {
        this.days = days;
    }

    public void setHours(long hours) {
        this.hours = hours;
    }

    public void setMinutes(long minutes) {
        this.minutes = minutes;
    }

    public void setSeconds(long seconds) {
        this.seconds = seconds;
    }

    public void setMillis(long millis) {
        this.millis = millis;
    }
}
