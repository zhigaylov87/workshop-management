package qwe.workshop.management.core.util;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;

/**
 * User: artem
 * Date: 08.07.15
 * Time: 4:29
 */
public class DateInterval implements Serializable {

    private Date startDate;
    private Date endDate;

    public DateInterval(Date startDate, Date endDate) {
        this.startDate = startDate;
        this.endDate = endDate;
    }

    public DateInterval(Date currentDate) {
        Calendar startCalendar = Calendar.getInstance();
        startCalendar.setTime(currentDate);
        startCalendar.set(Calendar.DAY_OF_MONTH, startCalendar.getActualMinimum(Calendar.DAY_OF_MONTH));
        startDate = startCalendar.getTime();

        endDate = currentDate;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public Date getStartDateBottom() {
        Calendar c = Calendar.getInstance();
        c.setTime(startDate);
        c.set(Calendar.HOUR_OF_DAY, 0);
        c.set(Calendar.MINUTE, 0);
        c.set(Calendar.SECOND, 0);
        c.set(Calendar.MILLISECOND, 0);
        return c.getTime();
    }

    public Date getEndDateTop() {
        Calendar c = Calendar.getInstance();
        c.setTime(endDate);
        c.set(Calendar.HOUR_OF_DAY, 23);
        c.set(Calendar.MINUTE, 59);
        c.set(Calendar.SECOND, 59);
        c.set(Calendar.MILLISECOND, 0);
        return c.getTime();
    }

    @Override
    public String toString() {
        return "DateInterval {" +
                "startDate: " + startDate +
                ", endDate: " + endDate +
                '}';
    }
}
