package qwe.workshop.management.web.bean.operation;

import org.primefaces.context.RequestContext;
import org.springframework.context.annotation.Scope;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;
import qwe.workshop.management.core.domain.operation.Operation;
import qwe.workshop.management.core.domain.operation.OperationCategory;
import qwe.workshop.management.core.domain.operation.OperationType;
import qwe.workshop.management.core.domain.operation.OrderSource;
import qwe.workshop.management.core.report.operation.OperationByTypeReport;
import qwe.workshop.management.core.service.ServiceException;
import qwe.workshop.management.core.service.api.OperationCategoryService;
import qwe.workshop.management.core.service.api.OperationReportService;
import qwe.workshop.management.core.service.api.OperationService;
import qwe.workshop.management.core.service.api.OrderSourceService;
import qwe.workshop.management.core.util.DateHelper;
import qwe.workshop.management.core.util.DateInterval;
import qwe.workshop.management.web.component.DateIntervalFilter;
import qwe.workshop.management.web.component.EntityListEditor;
import qwe.workshop.management.web.component.table.PaginationDataModel;
import qwe.workshop.management.web.converter.IdentifiableConverter;
import qwe.workshop.management.web.util.MessageManager;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.faces.context.FacesContext;
import javax.validation.ConstraintViolationException;
import java.io.Serializable;
import java.util.*;

/**
 * User: artem
 * Date: 04.07.15
 * Time: 7:26
 */
@Component("operationsBean")
@Scope("view")
public class OperationsBean implements PaginationDataModel.Handler<Operation>, EntityListEditor<Operation>, DateIntervalFilter, Serializable {

    private static final String BUNDLE_NAME = "qwe.workshop.management.web.i18n.operation.operations";

    @Resource(name = "operationService")
    private transient OperationService operationService;

    @Resource(name = "operationCategoryService")
    private transient OperationCategoryService operationCategoryService;

    @Resource(name = "orderSourceService")
    private transient OrderSourceService orderSourceService;

    @Resource(name = "operationReportService")
    private transient OperationReportService operationReportService;

    @Resource(name = "dateHelper")
    private transient DateHelper dateHelper;

    @Resource(name = "messageManager")
    private transient MessageManager messageManager;


    private PaginationDataModel<Operation> operationsModel = new PaginationDataModel<>(this);
    private DateInterval dateInterval;
    private OperationByTypeReport operationByTypeReport;

    private List<OperationType> operationTypes = Arrays.asList(OperationType.values());
    private List<OperationCategory> allOperationCategories;
    private Map<OperationType, List<OperationCategory>> operationTypeToCategories;
    private List<OrderSource> orderSources;
    private Operation preparedOperation;
    private OperationType preparedOperationType;
    private String editorDialogTitle;

    @PostConstruct
    private void init() {
        dateInterval = new DateInterval(dateHelper.getCurrentDate());
        filteringInvoked();

        allOperationCategories = operationCategoryService.findAll();
        operationTypeToCategories = new HashMap<>();
        for (OperationCategory category : allOperationCategories) {
            OperationType type = category.getType();
            List<OperationCategory> categories = operationTypeToCategories.get(type);
            if (categories == null) {
                categories = new ArrayList<>();
                operationTypeToCategories.put(type, categories);
            }
            categories.add(category);
        }

        orderSources = orderSourceService.findAll();
    }

    @Override
    public String getDataTableId() {
        return ":operationsForm:operationsTable";
    }

    @Override
    public Page<Operation> loadPage(Pageable pageable) {
        return operationService.find(pageable, dateInterval);
    }

    @Override
    public void deselect() {
        // Do nothing. No selection in table.
    }

    @Override
    public void filteringInvoked() {
        operationsModel.filteringInvoked();
        operationByTypeReport = operationReportService.getOperationByTypeReport(dateInterval);
    }

    @Override
    public void resetInvoked() {
        throw new UnsupportedOperationException("Reset not supported");
    }

    @Override
    public void prepareToAdd() {
        preparedOperation = new Operation();
        preparedOperation.setDate(dateHelper.getCurrentDate());
        preparedOperationType = null;
        editorDialogTitle = messageManager.getMessage(BUNDLE_NAME, "add_dialog_title");
    }

    @Override
    public void prepareToEdit(Operation entity) {
        preparedOperation = new Operation(entity);
        preparedOperationType = preparedOperation.getCategory().getType();
        editorDialogTitle = messageManager.getMessage(BUNDLE_NAME, "edit_dialog_title");
    }

    @Override
    public void save() {
        try {
            preparedOperation = operationService.save(preparedOperation);
        } catch (ConstraintViolationException e) {
            messageManager.addErrorMessage(e);
            FacesContext.getCurrentInstance().validationFailed();
            return;
        }

        messageManager.addInfoMessage(BUNDLE_NAME, "save_success", preparedOperation.getId());
        filteringInvoked();
        RequestContext.getCurrentInstance().update(Arrays.asList("entityListEditorTableForm", "messages"));
    }

    @Override
    public boolean canBeDelete(Operation entity) {
        return true; // TODO: Нужно ли проверять какие-то условия?
    }

    @Override
    public void prepareToDelete(Operation entity) {
        preparedOperation = entity;
        editorDialogTitle = messageManager.getMessage(BUNDLE_NAME, "delete_dialog_title", preparedOperation.getId());
    }

    @Override
    public void delete() {
        try {
            operationService.delete(preparedOperation.getId());
        } catch (ServiceException e) {
            messageManager.addErrorMessage(e);
            return;
        }

        messageManager.addInfoMessage(BUNDLE_NAME, "delete_success", preparedOperation.getId());
        filteringInvoked();
    }

    @Override
    public String getEditorDialogTitle() {
        return editorDialogTitle;
    }

    public void preparedOperationTypeChanged() {
        preparedOperation.setCategory(null);
    }

    // setters & getters

    @Override
    public DateInterval getDateInterval() {
        return dateInterval;
    }

    public OperationByTypeReport getOperationByTypeReport() {
        return operationByTypeReport;
    }

    public PaginationDataModel<Operation> getOperationsModel() {
        return operationsModel;
    }

    public Operation getPreparedOperation() {
        return preparedOperation;
    }

    public OperationType getPreparedOperationType() {
        return preparedOperationType;
    }

    public void setPreparedOperationType(OperationType preparedOperationType) {
        this.preparedOperationType = preparedOperationType;
    }

    public List<OperationType> getOperationTypes() {
        return operationTypes;
    }

    public List<OperationCategory> getPreparedOperationCategories() {
        return preparedOperationType == null ? null : operationTypeToCategories.get(preparedOperationType);
    }

    public IdentifiableConverter<OperationCategory> getOperationCategoryConverter() {
        return new IdentifiableConverter<>(allOperationCategories, true);
    }

    public List<OrderSource> getOrderSources() {
        return orderSources;
    }

    public IdentifiableConverter<OrderSource> getOrderSourceConverter() {
        return new IdentifiableConverter<>(orderSources, true);
    }

    // inner entities
}
