package qwe.workshop.management.core.service;

import com.fasterxml.jackson.annotation.JsonFormat;
import qwe.workshop.management.core.util.MutableTimeDuration;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

import static qwe.workshop.management.core.ResourcesUtils.TIMESTAMP_PATTERN;

/**
 * User: artem
 * Date: 18.07.15
 * Time: 7:34
 */
public class TaskTimeDuration {

    private Date creationTimestamp;

    private List<TaskStatusChangeTimestamp> statusChangeTimestamps;

    private Date currentTimestamp;

    private MutableTimeDuration timeDuration;

    @JsonFormat(pattern = TIMESTAMP_PATTERN)
    public Date getCreationTimestamp() {
        return creationTimestamp;
    }

    public void setCreationTimestamp(Date creationTimestamp) {
        this.creationTimestamp = creationTimestamp;
    }

    public List<TaskStatusChangeTimestamp> getStatusChangeTimestamps() {
        return statusChangeTimestamps;
    }

    public void setStatusChangeTimestamps(List<TaskStatusChangeTimestamp> statusChangeTimestamps) {
        this.statusChangeTimestamps = statusChangeTimestamps;
    }

    @JsonFormat(pattern = TIMESTAMP_PATTERN)
    public Date getCurrentTimestamp() {
        return currentTimestamp;
    }

    public void setCurrentTimestamp(Date currentTimestamp) {
        this.currentTimestamp = currentTimestamp;
    }

    public MutableTimeDuration getTimeDuration() {
        return timeDuration;
    }

    public void setTimeDuration(MutableTimeDuration timeDuration) {
        this.timeDuration = timeDuration;
    }

    @Override
    public String toString() {
        return "TaskTimeDuration {" +
                "creationTimestamp: " + creationTimestamp +
                ", statusChangeTimestamps: " + Arrays.toString(statusChangeTimestamps.toArray()) +
                ", currentTimestamp: " + currentTimestamp +
                ", timeDuration: " + timeDuration +
                '}';
    }
}
