package qwe.workshop.management.core.service.api;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import qwe.workshop.management.core.domain.operation.Operation;
import qwe.workshop.management.core.util.DateInterval;

import javax.validation.Valid;

/**
 * User: artem
 * Date: 04.07.15
 * Time: 7:14
 */
public interface OperationService {

    public Page<Operation> find(Pageable pageable, DateInterval interval);

    public Operation save(@Valid Operation operation);

    public void delete(long operationId);

}
