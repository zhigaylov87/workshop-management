package qwe.workshop.management.core.report.operation;

import java.io.Serializable;
import java.util.Collections;
import java.util.List;

/**
 * User: artem
 * Date: 12.07.15
 * Time: 2:53
 */
public class OperationByOrderSourceReport implements Serializable {

    private List<OperationByOrderSourceReportItem> items;

    public OperationByOrderSourceReport(List<OperationByOrderSourceReportItem> items) {
        this.items = Collections.unmodifiableList(items);
    }

    public List<OperationByOrderSourceReportItem> getItems() {
        return items;
    }

    @Override
    public String toString() {
        StringBuilder s = new StringBuilder("OperationByOrderSourceReport {items:\n");
        for (OperationByOrderSourceReportItem item : items) {
            s.append(item).append("\n");
        }
        return s.append('}').toString();
    }
}
