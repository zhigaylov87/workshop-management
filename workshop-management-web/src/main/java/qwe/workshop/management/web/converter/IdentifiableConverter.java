package qwe.workshop.management.web.converter;


import qwe.workshop.management.core.domain.Identifiable;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;
import java.util.Collection;

/**
 * User: artem
 * Date: 26.10.13
 * Time: 23:18
 */
public class IdentifiableConverter<T extends Identifiable> implements Converter {

    private static final long NULL_VALUE_ID = -1;

    private Collection<T> values;
    private boolean nullValueSupported = false;

    /**
     * Null values <b>not</b> supported
     * @param values values
     */
    public IdentifiableConverter(Collection<T> values) {
        this.values = values;
    }

    public IdentifiableConverter(Collection<T> values, boolean nullValueSupported) {
        this.values = values;
        this.nullValueSupported = nullValueSupported;
    }

    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String idStr) throws ConverterException {
        long id = Long.parseLong(idStr);

        if (nullValueSupported && id == NULL_VALUE_ID) {
            return null;
        }

        for (T value : values) {
            if (id == value.getId()) {
                return value;
            }
        }

        throw new ConverterException("Can not find value with id = " + id);
    }

    @SuppressWarnings(value = "unchecked")
    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value) throws ConverterException {
        if (value == null) {
            if (nullValueSupported) {
                return Long.toString(NULL_VALUE_ID);
            } else {
                throw new ConverterException("Value must be not null");
            }
        }

        if (!(value instanceof Identifiable)) {
            throw new ConverterException("Value must be an instance of " + Identifiable.class.getCanonicalName() +
            ". Actual: " + value.getClass().getCanonicalName());
        }

        T typedValue;
        try {
            typedValue = (T) value;
        } catch (ClassCastException e) {
            throw new ConverterException("Invalid value type", e);
        }

        return Long.toString(typedValue.getId());
    }
}
