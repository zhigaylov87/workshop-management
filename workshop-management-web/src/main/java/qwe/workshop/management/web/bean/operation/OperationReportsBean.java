package qwe.workshop.management.web.bean.operation;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import qwe.workshop.management.core.util.WebUtils;

import javax.annotation.PostConstruct;
import java.io.Serializable;
import java.util.Arrays;
import java.util.List;

/**
 * User: artem
 * Date: 22.06.15
 * Time: 1:31
 */
@Component("operationReportsBean")
@Scope("view")
public class OperationReportsBean implements Serializable {

    private static final int BY_TYPE_MODE_INDEX = 0;
    private static final int BY_CATEGORY_MODE_INDEX = 1;
    private static final int BY_MONTH_MODE_INDEX = 2;
    private static final int BY_ORDER_SOURCE_MODE_INDEX = 3;

    private static final List<Integer> MODES = Arrays.asList(
            BY_TYPE_MODE_INDEX, BY_CATEGORY_MODE_INDEX, BY_MONTH_MODE_INDEX, BY_ORDER_SOURCE_MODE_INDEX);

    private int modeIndex;

    @PostConstruct
    private void init() {
        Integer modeParam = WebUtils.getIntegerRequestParameter("mode");
        modeIndex = modeParam == null ? BY_TYPE_MODE_INDEX : modeParam;
        if (!MODES.contains(modeIndex)) {
            throw new IllegalArgumentException("Illegal mode index: " + modeIndex);
        }
    }

    // getters

    public int getByTypeModeIndex() {
        return BY_TYPE_MODE_INDEX;
    }

    public int getByCategoryModeIndex() {
        return BY_CATEGORY_MODE_INDEX;
    }

    public int getByMonthModeIndex() {
        return BY_MONTH_MODE_INDEX;
    }

    public int getByOrderSourceModeIndex() {
        return BY_ORDER_SOURCE_MODE_INDEX;
    }

    public boolean isByTypeMode() {
        return modeIndex == BY_TYPE_MODE_INDEX;
    }

    public boolean isByCategoryMode() {
        return modeIndex == BY_CATEGORY_MODE_INDEX;
    }

    public boolean isByMonthMode() {
        return modeIndex == BY_MONTH_MODE_INDEX;
    }

    public boolean isByOrderSourceMode() {
        return modeIndex == BY_ORDER_SOURCE_MODE_INDEX;
    }

}
