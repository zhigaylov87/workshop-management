package qwe.workshop.management.web.converter;

import qwe.workshop.management.core.util.FormatUtils;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;
import javax.faces.convert.FacesConverter;

/**
 * User: artem
 * Date: 02.09.14
 * Time: 1:18
 * @see qwe.workshop.management.core.util.FormatUtils#formatAmount(int)
 */
@FacesConverter(value = "AmountConverter")
public class AmountConverter implements Converter {

    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String value) {
        throw new UnsupportedOperationException("AmountConverter is not working in this direction!");
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value) {
        if (!(value instanceof Integer)) {
            throw new ConverterException("Value must be an instance of " + Integer.class.getName() +
                    ". Value: " + value +
                    ", value class: " + (value == null ? null : value.getClass().getName()));
        }
        int amount = (Integer) value;
        return FormatUtils.formatAmount(amount);
    }
}
