package qwe.workshop.management.web.util;

import org.springframework.stereotype.Component;

import java.text.MessageFormat;

/**
 * There is no varargs support in EL, therefore this some ugly bean is using
 *
 * User: artem
 * Date: 29.03.14
 * Time: 19:27
 */
@Component("messageFormater")
public class MessageFormater {

    public String format(String pattern, Object arg1) {
        return MessageFormat.format(pattern, arg1);
    }

    public String format(String pattern, Object arg1, Object arg2) {
        return MessageFormat.format(pattern, arg1, arg2);
    }

    public String format(String pattern, Object arg1, Object arg2, Object arg3) {
        return MessageFormat.format(pattern, arg1, arg2, arg3);
    }

    public String format(String pattern, Object arg1, Object arg2, Object arg3, Object arg4) {
        return MessageFormat.format(pattern, arg1, arg2, arg3, arg4);
    }

    public String format(String pattern, Object arg1, Object arg2, Object arg3, Object arg4, Object arg5) {
        return MessageFormat.format(pattern, arg1, arg2, arg3, arg4, arg5);
    }

}
