package qwe.workshop.management.core.service.impl;

import com.google.common.collect.Lists;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import qwe.workshop.management.core.CoreException;
import qwe.workshop.management.core.domain.TimeDuration;
import qwe.workshop.management.core.domain.task.Task;
import qwe.workshop.management.core.domain.task.TaskInterval;
import qwe.workshop.management.core.domain.task.TaskStatus;
import qwe.workshop.management.core.repository.TaskIntervalRepository;
import qwe.workshop.management.core.repository.TaskRepository;
import qwe.workshop.management.core.service.api.TaskService;
import qwe.workshop.management.core.util.DateHelper;

import javax.annotation.Resource;
import javax.validation.Valid;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Set;

/**
 * User: artem
 * Date: 20.06.15
 * Time: 4:23
 */
@Service("taskService")
@Repository
@Transactional
public class TaskServiceImpl implements TaskService {

    private static final Logger log = LoggerFactory.getLogger(TaskServiceImpl.class);

    private static final List<TaskStatus> CAN_BE_DELETE_STATUSES = Arrays.asList(TaskStatus.PLANNED);
    private static final List<TaskStatus> CAN_BE_START_STATUSES = Arrays.asList(TaskStatus.PLANNED, TaskStatus.PAUSED);
    private static final List<TaskStatus> CAN_BE_PAUSE_STATUSES = Arrays.asList(TaskStatus.ACTIVE);
    private static final List<TaskStatus> CAN_BE_FINISH_STATUSES = Arrays.asList(TaskStatus.ACTIVE, TaskStatus.PAUSED);

    @Autowired
    private TaskRepository taskRepository;

    @Autowired
    private TaskIntervalRepository taskIntervalRepository;

    @Resource(name = "dateHelper")
    private DateHelper dateHelper;

    @Transactional(readOnly = true)
    @Override
    public List<Task> findCurrent() {
        String logMsg = " of loading current tasks";
        try {
            log.info("Start" + logMsg);
            List<Task> tasks = calculateAndSetExecutionPeriod(Lists.newArrayList(taskRepository.findCurrent()));
            log.info("End" + logMsg + ". Total count: " + tasks.size());
            return tasks;
        } catch (Exception e) {
            log.error("Error" + logMsg, e);
            throw new CoreException(e);
        }
    }

    @Transactional(readOnly = true)
    @Override
    public Page<Task> findFinished(Pageable pageable) {
        String logMsg = " of loading finished tasks. Page number: " + pageable.getPageNumber() +
                ", page size: " + pageable.getPageSize();
        try {
            log.info("Start" + logMsg);
            Page<Task> page = taskRepository.findFinished(pageable);
            calculateAndSetExecutionPeriod(page.getContent());
            log.info("End" + logMsg + ". Total count: " + page.getTotalElements() +
                    ", page count: " + page.getNumberOfElements());
            return page;
        } catch (Exception e) {
            log.error("Error" + logMsg, e);
            throw new CoreException(e);
        }
    }

    @Override
    public Task save(@Valid Task task) {
        String logMsg = " of saving task: " + task;
        try {
            log.info("Start" + logMsg);
            if (task.getId() == null) {
                task.setStatus(TaskStatus.PLANNED);
                task.setStatusTimestamp(dateHelper.getCurrentDate());
            } else {
                Task persistenceTask = taskRepository.findOne(task.getId());
                if (persistenceTask.getStatus() != task.getStatus()) {
                    throw new CoreException("Task status can not be modified on save. Persistence task: " + persistenceTask);
                }
                if (!persistenceTask.getStatusTimestamp().equals(task.getStatusTimestamp())) {
                    throw new CoreException("Task status timestamp can not be modified on save. Persistence task: " + persistenceTask);
                }
            }

            task = taskRepository.save(task);
            log.info("End" + logMsg + ". Saved task: " + task);
            return task;
        } catch (Exception e) {
            log.error("Error" + logMsg, e);
            throw new CoreException(e);
        }
    }

    @Override
    public boolean canBeDelete(TaskStatus taskStatus) {
        return CAN_BE_DELETE_STATUSES.contains(taskStatus);
    }

    @Override
    public void delete(long taskId) {
        String logMsg = " of deleting task. Task id: " + taskId;
        try {
            log.info("Start" + logMsg);
            Task task = taskRepository.findOne(taskId);
            if (!canBeDelete(task.getStatus())) {
                throwIllegalTaskStatus(task.getStatus());
            }
            log.info("Processing" + logMsg + ". Task: " + task);
            taskRepository.delete(task);
            log.info("End" + logMsg);
        } catch (Exception e) {
            log.error("Error" + logMsg, e);
            throw new CoreException(e);
        }
    }

    @Override
    public boolean canChangeStatus(TaskStatus currentStatus, TaskStatus newStatus) {
        switch (newStatus) {
            case PLANNED:
                return false;
            case ACTIVE:
                return CAN_BE_START_STATUSES.contains(currentStatus);
            case PAUSED:
                return CAN_BE_PAUSE_STATUSES.contains(currentStatus);
            case FINISHED:
                return CAN_BE_FINISH_STATUSES.contains(currentStatus);
            default:
                throw new CoreException("Unknown new task status: " + newStatus);
        }
    }

    @Override
    public void changeStatus(long taskId, TaskStatus newStatus) {
        String logMsg = " of changing task status. Task id: " + taskId + ", new status: " + newStatus;
        try {
            log.info("Start" + logMsg);
            Task task = taskRepository.findOne(taskId);
            log.info("Processing" + logMsg + ". Task: " + task);

            if (newStatus == TaskStatus.ACTIVE) {
                List<Task> activeTasks = taskRepository.findActive();
                if (activeTasks.size() > 1) {
                    throw new CoreException("More than one active tasks: " +
                            Arrays.toString(activeTasks.toArray(new Task[activeTasks.size()])));
                }
                if (activeTasks.size() == 1) {
                    Task activeTask = activeTasks.get(0);
                    log.info("Processing" + logMsg + ". Pausing current active task: " + activeTask);
                    changeStatus(activeTask, TaskStatus.PAUSED);
                }
            }

            changeStatus(task, newStatus);

            log.info("End" + logMsg);
        } catch (Exception e) {
            log.error("Error" + logMsg, e);
            throw new CoreException(e);
        }
    }

    private void changeStatus(Task task, TaskStatus newStatus) {
        if (!canChangeStatus(task.getStatus(), newStatus)) {
            throwIllegalTaskStatus(task.getStatus());
        }

        Date currentTimestamp = dateHelper.getCurrentDate();
        if (task.getStatus() == TaskStatus.ACTIVE) {
            TaskInterval taskInterval = new TaskInterval();
            taskInterval.setStartTimestamp(task.getStatusTimestamp());
            taskInterval.setEndTimestamp(currentTimestamp);
            taskInterval.setTask(task);
            taskIntervalRepository.save(taskInterval);
        }
        task.setStatus(newStatus);
        task.setStatusTimestamp(currentTimestamp);
        taskRepository.save(task);
    }

    private void throwIllegalTaskStatus(TaskStatus status) {
        throw new CoreException("Illegal task status: " + status);
    }

    private List<Task> calculateAndSetExecutionPeriod(List<Task> tasks) {
        for (Task task : tasks) {
            calculateAndSetExecutionPeriod(task);
        }
        return tasks;
    }

    private void calculateAndSetExecutionPeriod(Task task) {
        long totalMillis = TimeDuration.ZERO;

        Set<TaskInterval> intervals = task.getIntervals();
        if (intervals != null && !intervals.isEmpty()) {
            for (TaskInterval taskInterval : intervals) {
                totalMillis += taskInterval.getEndTimestamp().getTime() - taskInterval.getStartTimestamp().getTime();
            }
        }

        if (task.getStatus() == TaskStatus.ACTIVE) {
            totalMillis += dateHelper.getCurrentDate().getTime() - task.getStatusTimestamp().getTime();
        }

        task.setDuration(new TimeDuration(totalMillis));
    }
}
