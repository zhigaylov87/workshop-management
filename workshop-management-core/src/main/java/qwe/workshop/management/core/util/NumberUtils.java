package qwe.workshop.management.core.util;

import java.math.BigDecimal;
import java.math.BigInteger;

/**
 * User: artem
 * Date: 11.07.15
 * Time: 5:13
 */
public class NumberUtils {

    public static long toLong(BigInteger bigInteger) {
        return Long.parseLong(bigInteger.toString());
    }

    public static long toLong(BigDecimal bigDecimal) {
        return Long.parseLong(bigDecimal.toString());
    }

    public static int toInt(BigInteger bigInteger) {
        return Integer.parseInt(bigInteger.toString());
    }

}
