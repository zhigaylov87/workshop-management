package qwe.workshop.management.core.domain.task;

import org.hibernate.validator.constraints.NotEmpty;
import qwe.workshop.management.core.domain.Identifiable;
import qwe.workshop.management.core.domain.TimeDuration;
import qwe.workshop.management.core.util.FormatUtils;
import qwe.workshop.management.core.util.ObjectUtils;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Date;
import java.util.Set;

/**
 * User: artem
 * Date: 18.06.15
 * Time: 4:02
 */
@Entity
@Table(name = "TASK")
public class Task implements Identifiable, Serializable {

    private Long id;
    private TaskType type;
    private String name;
    private String comment;
    private Date taskDate;
    private TaskStatus status;
    private Date statusTimestamp;
    private Set<TaskInterval> intervals;
    private TimeDuration duration;

    public Task() {
    }

    public Task(Task other) {
        this.id = other.id;
        this.type = other.type;
        this.name = other.name;
        this.comment = other.comment;
        this.taskDate = other.taskDate;
        this.status = other.status;
        this.statusTimestamp = other.statusTimestamp;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @NotNull(message = "{Task.type.NotNull}")
    @ManyToOne(optional = false)
    @JoinColumn(name = "TASK_TYPE_ID")
    public TaskType getType() {
        return type;
    }

    public void setType(TaskType type) {
        this.type = type;
    }

    @NotEmpty(message = "{Task.name.NotEmpty}")
    @Size(max = 128, message = "{Task.name.Size}")
    @Column(name = "NAME", nullable = false)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Size(max = 512, message = "{Task.comment.Size}")
    @Column(name = "COMMENT")
    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    @Column(name = "TASK_DATE")
    @Temporal(TemporalType.DATE)
    public Date getTaskDate() {
        return taskDate;
    }

    public void setTaskDate(Date taskDate) {
        this.taskDate = taskDate;
    }

    @NotNull(message = "{Task.status.NotNull}")
    @Column(name = "STATUS")
    @Enumerated(EnumType.STRING)
    public TaskStatus getStatus() {
        return status;
    }

    public void setStatus(TaskStatus status) {
        this.status = status;
    }

    @NotNull(message = "{Task.statusTimestamp.NotNull}")
    @Column(name = "STATUS_TIMESTAMP")
    @Temporal(TemporalType.TIMESTAMP)
    public Date getStatusTimestamp() {
        return statusTimestamp;
    }

    public void setStatusTimestamp(Date statusTimestamp) {
        this.statusTimestamp = statusTimestamp;
    }

    @OneToMany(mappedBy = "task", fetch = FetchType.LAZY)
    public Set<TaskInterval> getIntervals() {
        return intervals;
    }

    public void setIntervals(Set<TaskInterval> intervals) {
        this.intervals = intervals;
    }

    @Transient
    public TimeDuration getDuration() {
        return duration;
    }

    public void setDuration(TimeDuration duration) {
        this.duration = duration;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Task)) return false;

        Task task = (Task) o;
        return ObjectUtils.equalsIds(this, task);
    }

    @Override
    public int hashCode() {
        return id != null ? id.hashCode() : 0;
    }

    @Override
    public String toString() {
        return "Task {" +
                "id: " + id +
                ", type: " + type +
                ", name: " + FormatUtils.singleQuotes(name) +
                ", comment: " + FormatUtils.singleQuotes(comment) +
                ", taskDate: " + taskDate +
                ", status: " + status +
                ", statusTimestamp: " + statusTimestamp +
                ", duration: " + duration +
                '}';
    }
}
