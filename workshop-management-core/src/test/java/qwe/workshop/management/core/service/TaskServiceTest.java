package qwe.workshop.management.core.service;

import org.junit.Test;
import org.springframework.data.domain.PageRequest;
import qwe.workshop.management.core.ResourcesUtils;
import qwe.workshop.management.core.domain.task.Task;
import qwe.workshop.management.core.domain.task.TaskStatus;
import qwe.workshop.management.core.domain.task.TaskType;
import qwe.workshop.management.core.service.api.TaskService;
import qwe.workshop.management.core.util.IdentifiableList;
import qwe.workshop.management.core.util.MutableDateHelper;
import qwe.workshop.management.core.util.MutableTimeDuration;

import javax.annotation.Resource;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static qwe.workshop.management.core.JsonResource.ValidTaskStatusToStatusChanges;
import static qwe.workshop.management.core.JsonResource.ValidTaskTimeDurations;

/**
 * User: artem
 * Date: 18.07.15
 * Time: 4:23
 */
public class TaskServiceTest extends AbstractServiceTest {

    @Resource(name = "taskService")
    private TaskService taskService;

    @Resource(name = "testDateHelper")
    private MutableDateHelper dateHelper;

    @Test
    public void testTaskStatusToStatusChange() {

        List<TaskStatusToStatusChange> validChanges = ResourcesUtils.getList(ValidTaskStatusToStatusChanges, TaskStatusToStatusChange.class);

        for (TaskStatus currentStatus : TaskStatus.values()) {
            for (TaskStatus newStatus : TaskStatus.values()) {
                TaskStatusToStatusChange change = new TaskStatusToStatusChange(currentStatus, newStatus);
                boolean expected = validChanges.contains(change);
                boolean actual = taskService.canChangeStatus(change.getCurrentStatus(), change.getNewStatus());
                assertEquals("Change: " + change, expected, actual);
            }
        }
    }

    @Test
    public void testTaskTimeDuration() {
        List<TaskTimeDuration> taskTimeDurations = ResourcesUtils.getList(ValidTaskTimeDurations, TaskTimeDuration.class);

        for (TaskTimeDuration taskTimeDuration : taskTimeDurations) {
            dateHelper.setDate(taskTimeDuration.getCreationTimestamp());
            Task task = newSavedFooTask(newSavedFooTaskType());
            TaskStatus lastStatus = task.getStatus();

            for (TaskStatusChangeTimestamp statusChangeTimestamp : taskTimeDuration.getStatusChangeTimestamps()) {
                dateHelper.setDate(statusChangeTimestamp.getTimestamp());
                TaskStatus status = statusChangeTimestamp.getStatus();
                taskService.changeStatus(task.getId(), status);
                lastStatus = status;
            }

            dateHelper.setDate(taskTimeDuration.getCurrentTimestamp());
            List<Task> tasks = lastStatus == TaskStatus.FINISHED ?
                    taskService.findFinished(new PageRequest(0, Integer.MAX_VALUE)).getContent() :
                    taskService.findCurrent();
            task = new IdentifiableList<>(tasks).getById(task.getId());

            assertEquals("TaskTimeDuration: " + taskTimeDuration, taskTimeDuration.getTimeDuration(), task.getDuration());
        }
    }

    @Test
    public void testActiveTaskPauseOnActivateOtherTask() {
        TaskType taskType = newSavedFooTaskType();
        Task firstTask = newSavedFooTask(taskType);
        Task secondTask = newSavedFooTask(taskType);

        setCurrentDate("2015-07-01 09:50:42");
        taskService.changeStatus(firstTask.getId(), TaskStatus.ACTIVE);

        setCurrentDate("2015-07-01 13:00:00");
        taskService.changeStatus(secondTask.getId(), TaskStatus.ACTIVE);

        setCurrentDate("2015-07-01 18:42:37");
        IdentifiableList<Task> tasks = new IdentifiableList<>(taskService.findCurrent());
        firstTask = tasks.getById(firstTask.getId());
        secondTask = tasks.getById(secondTask.getId());

        assertEquals(TaskStatus.PAUSED, firstTask.getStatus());
        assertEquals(new MutableTimeDuration(0, 3, 9, 18), firstTask.getDuration());
        assertEquals(new MutableTimeDuration(0, 5, 42, 37), secondTask.getDuration());
    }
}
