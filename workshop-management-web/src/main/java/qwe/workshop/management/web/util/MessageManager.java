package qwe.workshop.management.web.util;

import org.springframework.context.MessageSource;
import org.springframework.stereotype.Component;
import qwe.workshop.management.core.service.ServiceException;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import java.text.MessageFormat;
import java.util.Locale;
import java.util.ResourceBundle;

/**
 * User: artem
 * Date: 26.10.13
 * Time: 0:45
 */
@Component("messageManager")
public class MessageManager {

    private static final String MONTH_NAME_BUNDLE = "qwe.workshop.management.web.i18n.month_name";
    private static final String COMMON_BUNDLE = "qwe.workshop.management.web.i18n.common";

    private static final String MESSAGES_ID = "messages";
    private static final Locale LOCALE = new Locale("ru", "RU");

//    @Resource(name = "serviceMessageSource") TODO
    private MessageSource serviceMessageSource;

    public void addErrorMessage(ConstraintViolationException e) {
        for (ConstraintViolation constraintViolation : e.getConstraintViolations()) {
            addErrorMessage(constraintViolation.getMessage());
        }
    }

    public void addErrorMessage(ServiceException e) {
        addErrorMessage(serviceMessageSource.getMessage(e.getMessageSourceResolvable(), getLocale()));
    }

    public void addInfoMessage(String bundleName, String bundleKey, Object... params) {
        String message = getMessage(bundleName, bundleKey, params);
        addMessage(FacesMessage.SEVERITY_INFO, message);
    }

    public void addErrorMessage(String bundleName, String bundleKey, Object... params) {
        addErrorMessage(getMessage(bundleName, bundleKey, params));
    }

    public String getMessage(String bundleName, String bundleKey, Object... params) {
        String message = getResourceBundleString(bundleName, bundleKey);
        return MessageFormat.format(message, params);
    }

    /**
     * @param month month (1-12)
     * @return month name
     */
    public String getMonthName(int month) {
        if (month < 1 || month > 12) {
            throw new IllegalArgumentException("Illegal month: " + month + ". Month must be from 1 to 12");
        }
        return getMessage(MONTH_NAME_BUNDLE, "month_" + month);
    }

    public String getCommon(String bundleKey, Object... params) {
        return getMessage(COMMON_BUNDLE, bundleKey);
    }

    private FacesMessage createMessage(FacesMessage.Severity severity, String message) {
        FacesMessage facesMessage = new FacesMessage(message);
        facesMessage.setSeverity(severity);
        return facesMessage;
    }

    private void addMessage(FacesMessage.Severity severity, String message) {
        FacesContext.getCurrentInstance().addMessage(MESSAGES_ID, createMessage(severity, message));
    }

    private void addErrorMessage(String message) {
        addMessage(FacesMessage.SEVERITY_ERROR, message);
    }

    private String getResourceBundleString(String bundleName, String bundleKey) {
        return ResourceBundle.getBundle(bundleName, getLocale()).getString(bundleKey);
    }

    private Locale getLocale() {
        return LOCALE;
    }

}
