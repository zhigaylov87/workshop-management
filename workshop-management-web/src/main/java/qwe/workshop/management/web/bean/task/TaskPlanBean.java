package qwe.workshop.management.web.bean.task;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import qwe.workshop.management.core.util.WebUtils;

import javax.annotation.PostConstruct;
import java.io.Serializable;

/**
 * User: artem
 * Date: 22.06.15
 * Time: 1:31
 */
@Component("taskPlanBean")
@Scope("view")
public class TaskPlanBean implements Serializable {

    private static final int CURRENT_MODE_INDEX = 0;
    private static final int FINISHED_MODE_INDEX = 1;

    private int modeIndex;

    @PostConstruct
    private void init() {
        Integer modeParam = WebUtils.getIntegerRequestParameter("mode");
        modeIndex = modeParam == null ? CURRENT_MODE_INDEX : modeParam;
        if (modeIndex != CURRENT_MODE_INDEX && modeIndex != FINISHED_MODE_INDEX) {
            throw new IllegalArgumentException("Illegal mode index: " + modeIndex);
        }
    }

    // getters

    public int getCurrentModeIndex() {
        return CURRENT_MODE_INDEX;
    }

    public int getFinishedModeIndex() {
        return FINISHED_MODE_INDEX;
    }

    public boolean isCurrentMode() {
        return modeIndex == CURRENT_MODE_INDEX;
    }

    public boolean isFinishedMode() {
        return modeIndex == FINISHED_MODE_INDEX;
    }

}
