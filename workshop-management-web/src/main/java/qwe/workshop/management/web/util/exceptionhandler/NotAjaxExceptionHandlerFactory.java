package qwe.workshop.management.web.util.exceptionhandler;

import javax.faces.context.ExceptionHandler;
import javax.faces.context.ExceptionHandlerFactory;

/**
 * User: artem
 * Date: 14.02.14
 * Time: 1:27
 */
public class NotAjaxExceptionHandlerFactory extends ExceptionHandlerFactory {

    private ExceptionHandlerFactory parent;

    public NotAjaxExceptionHandlerFactory(ExceptionHandlerFactory parent) {
        this.parent = parent;
    }

    @Override
    public ExceptionHandler getExceptionHandler() {
        return new NotAjaxExceptionHandler(parent.getExceptionHandler());

    }
}
