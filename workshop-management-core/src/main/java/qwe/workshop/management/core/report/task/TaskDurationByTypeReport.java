package qwe.workshop.management.core.report.task;

import qwe.workshop.management.core.domain.TimeDuration;
import qwe.workshop.management.core.domain.task.TaskType;

import java.io.Serializable;
import java.util.*;

/**
 * User: artem
 * Date: 11.07.15
 * Time: 3:08
 */
public class TaskDurationByTypeReport implements Serializable {

    private LinkedHashMap<TaskType, TimeDuration> typeToDuration = new LinkedHashMap<>();

    public void add(TaskType type, TimeDuration duration) {
        Objects.requireNonNull(type);
        Objects.requireNonNull(duration);
        if (typeToDuration.containsKey(type)) {
            throw new IllegalArgumentException("Report already contains task type " + type);
        }
        typeToDuration.put(type, duration);
    }

    public List<TaskType> getTypes() {
        return new ArrayList<>(typeToDuration.keySet());
    }

    public TimeDuration getDuration(TaskType type) {
        TimeDuration duration = typeToDuration.get(type);
        if (duration == null) {
            throw new IllegalArgumentException("There is no duration for type " + type);
        }
        return duration;
    }

    @Override
    public String toString() {
        StringBuilder s = new StringBuilder("TaskDurationByTypeReport {\n");
        for (Map.Entry<TaskType, TimeDuration> entry : typeToDuration.entrySet()) {
            s.append("{type: ").append(entry.getKey())
                    .append(", duration: ").append(entry.getValue()).append("\n");
        }
        return s.append("}").toString();
    }
}
