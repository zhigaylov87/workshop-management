package qwe.workshop.management.core.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import qwe.workshop.management.core.CoreException;
import qwe.workshop.management.core.domain.operation.OrderSource;
import qwe.workshop.management.core.repository.OrderSourceRepository;
import qwe.workshop.management.core.service.api.OrderSourceService;

import javax.validation.Valid;
import java.util.List;

/**
 * User: artem
 * Date: 12.07.15
 * Time: 1:22
 */
@Service("orderSourceService")
@Repository
@Transactional
public class OrderSourceServiceImpl implements OrderSourceService {

    private static final Logger log = LoggerFactory.getLogger(OrderSourceServiceImpl.class);

    @Autowired
    private OrderSourceRepository orderSourceRepository;

    @Transactional(readOnly = true)
    @Override
    public List<OrderSource> findAll() {
        String logMsg = " of loading all order sources";
        try {
            log.info("Start" + logMsg);
            List<OrderSource> all = orderSourceRepository.findAll();
            log.info("End" + logMsg + ". Total count: " + all.size());
            return all;
        } catch (Exception e) {
            log.error("Error" + logMsg, e);
            throw new CoreException(e);
        }
    }

    @Override
    public OrderSource save(@Valid OrderSource orderSource) {
        String logMsg = " of saving order source: " + orderSource;
        try {
            log.info("Start" + logMsg);
            orderSource = orderSourceRepository.save(orderSource);
            log.info("End" + logMsg + ". Saved order source: " + orderSource);
            return orderSource;
        } catch (Exception e) {
            log.error("Error" + logMsg, e);
            throw new CoreException(e);
        }
    }

    @Override
    public void delete(long orderSourceId) {
        String logMsg = " of deleting order source. Order source id: " + orderSourceId;
        try {
            log.info("Start" + logMsg);
            // TODO: Проверять что нет операций с таким источником заказа
            OrderSource orderSource = orderSourceRepository.findOne(orderSourceId);
            log.info("Processing" + logMsg + ". Order source: " + orderSource);
            orderSourceRepository.delete(orderSource);
            log.info("End" + logMsg);
        } catch (Exception e) {
            log.error("Error" + logMsg, e);
            throw new CoreException(e);
        }
    }
}
