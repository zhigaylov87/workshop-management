package qwe.workshop.management.core.service.impl;

import com.google.common.collect.Lists;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import qwe.workshop.management.core.CoreException;
import qwe.workshop.management.core.domain.operation.OperationCategory;
import qwe.workshop.management.core.repository.OperationCategoryRepository;
import qwe.workshop.management.core.service.api.OperationCategoryService;

import javax.validation.Valid;
import java.util.List;

/**
 * User: artem
 * Date: 04.07.15
 * Time: 5:40
 */
@Service("operationCategoryService")
@Repository
@Transactional
public class OperationCategoryServiceImpl implements OperationCategoryService {

    private static final Logger log = LoggerFactory.getLogger(OperationCategoryServiceImpl.class);

    @Autowired
    private OperationCategoryRepository operationCategoryRepository;

    @Transactional(readOnly = true)
    @Override
    public List<OperationCategory> findAll() {
        String logMsg = " of loading all operation categories";
        try {
            log.info("Start" + logMsg);
            List<OperationCategory> all = Lists.newArrayList(operationCategoryRepository.findAll());
            log.info("End" + logMsg + ". Total count: " + all.size());
            return all;
        } catch (Exception e) {
            log.error("Error" + logMsg, e);
            throw new CoreException(e);
        }
    }

    @Override
    public OperationCategory save(@Valid OperationCategory category) {
        String logMsg = " of saving operation category: " + category;
        try {
            log.info("Start" + logMsg);
            category = operationCategoryRepository.save(category);
            log.info("End" + logMsg + ". Saved operation category: " + category);
            return category;
        } catch (Exception e) {
            log.error("Error" + logMsg, e);
            throw new CoreException(e);
        }
    }

    @Override
    public void delete(long categoryId) {
        String logMsg = " of deleting operation category. Category id: " + categoryId;
        try {
            log.info("Start" + logMsg);
            // TODO: Проверять что нет операций с такой категорией
            OperationCategory category = operationCategoryRepository.findOne(categoryId);
            log.info("Processing" + logMsg + ". Category: " + category);
            operationCategoryRepository.delete(category);
            log.info("End" + logMsg);
        } catch (Exception e) {
            log.error("Error" + logMsg, e);
            throw new CoreException(e);
        }
    }
}
