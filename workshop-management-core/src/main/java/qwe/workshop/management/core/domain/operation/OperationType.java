package qwe.workshop.management.core.domain.operation;

/**
 * User: artem
 * Date: 01.07.15
 * Time: 2:57
 */
public enum OperationType {

    EXPENSE,
    INCOME

}
