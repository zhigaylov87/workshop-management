package qwe.workshop.management.core.validation.constraint;

import qwe.workshop.management.core.validation.validator.OperationValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

/**
 * User: artem
 * Date: 03.01.15
 * Time: 4:49
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
@Constraint(validatedBy = OperationValidator.class)
@Documented
public @interface OperationConstraint {

    String message() default "Operation.invalid";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};

}
