package qwe.workshop.management.core.report.operation;

import java.io.Serializable;

/**
 * User: artem
 * Date: 05.07.15
 * Time: 19:16
 */
public class OperationByTypeReport implements Serializable {

    private OperationReportAmounts reportAmounts;

    public OperationByTypeReport(OperationReportAmounts reportAmounts) {
        this.reportAmounts = reportAmounts;
    }

    public OperationReportAmounts getReportAmounts() {
        return reportAmounts;
    }

    @Override
    public String toString() {
        return "OperationByTypeReport {" +
                "reportAmounts: " + reportAmounts +
                '}';
    }
}
