package qwe.workshop.management.core.util;

import qwe.workshop.management.core.domain.Identifiable;

/**
 * User: artem
 * Date: 23.06.15
 * Time: 4:20
 */
public final class ObjectUtils {

    private ObjectUtils() {
    }

    public static boolean equalsIds(Identifiable a, Identifiable b) {
        return a.getId() != null && b.getId() != null && a.getId().equals(b.getId());
    }
}
