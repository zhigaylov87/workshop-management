package qwe.workshop.management.web.component.table;

import com.google.common.primitives.Ints;
import org.primefaces.component.datatable.DataTable;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortMeta;
import org.primefaces.model.SortOrder;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import qwe.workshop.management.core.domain.Identifiable;
import qwe.workshop.management.web.component.FilterActionListener;

import javax.faces.context.FacesContext;
import java.io.Serializable;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * User: artem
 * Date: 06.03.15
 * Time: 21:41
 */
public class PaginationDataModel<T extends Identifiable> extends LazyDataModel<T> implements FilterActionListener, Serializable {

    private List<T> pageItems;

    private boolean filtering = false;

    private Handler<T> handler;

    public PaginationDataModel(Handler<T> handler) {
        this.handler = Objects.requireNonNull(handler);
    }

    @Override
    public void filteringInvoked() {
        filtering = true;
    }

    @Override
    public void resetInvoked() {
        filtering = true;
    }

    public void paginationInvoked() {
        handler.deselect();
    }

    @Override
    public List<T> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters) {
        if (filtering) {
            // go to first page
            first = 0;
            DataTable dataTable = (DataTable) FacesContext.getCurrentInstance().getViewRoot()
                    .findComponent(handler.getDataTableId());
            dataTable.setFirst(first);
            filtering = false;
        }

        handler.deselect();

        int pageNumber = first / pageSize;
        Page<T> page = handler.loadPage(new PageRequest(pageNumber, pageSize));
        setRowCount(Ints.checkedCast(page.getTotalElements()));
        pageItems = page.getContent();
        return pageItems;
    }

    @Override
    public List<T> load(int first, int pageSize, List<SortMeta> multiSortMeta, Map<String, Object> filters) {
        throw new UnsupportedOperationException("Multisort loading not implemented!");
    }

    @Override
    public T getRowData(String rowKey) {
        long itemId = Long.parseLong(rowKey);
        for (T item : pageItems) {
            if (itemId == item.getId()) {
                return item;
            }
        }

        return null;
//            throw new IllegalArgumentException("Product with id = " + productId + " not found on current page!");
    }

    @Override
    public Object getRowKey(T item) {
        return item.getId();
    }

    public interface Handler<T extends Identifiable> {

        public String getDataTableId();

        public Page<T> loadPage(Pageable pageable);

        public void deselect();
    }

}