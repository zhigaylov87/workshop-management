package qwe.workshop.management.core.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import qwe.workshop.management.core.domain.task.Task;

import java.util.Date;
import java.util.List;

/**
 * User: artem
 * Date: 19.06.15
 * Time: 5:29
 */
public interface TaskReportRepository extends CrudRepository<Task, Long> {

    @Query(nativeQuery = true,
            value = "select tt.id, sum(ti.interval_seconds) as total_seconds from " +
                    "(select task_id, " +
                        "(case " +
                            "when start_timestamp < :startTimestamp and (end_timestamp between :startTimestamp and :endTimestamp) " +
                            "then datediff('SECOND', :startTimestamp, end_timestamp) " +

                            "when (start_timestamp between :startTimestamp and :endTimestamp) and end_timestamp > :endTimestamp " +
                            "then datediff('SECOND', start_timestamp, :endTimestamp) " +

                            "when start_timestamp >= :startTimestamp and end_timestamp <= :endTimestamp " +
                            "then datediff('SECOND', start_timestamp, end_timestamp) " +

                            "else datediff('SECOND', :startTimestamp, :endTimestamp) " +
                        "end) as interval_seconds " +
                    "from " +
                          "(select task_id, start_timestamp, end_timestamp from task_interval " +
                           "where (start_timestamp between :startTimestamp and :endTimestamp) or " +
                                 "(end_timestamp between :startTimestamp and :endTimestamp) or " +
                                 "(start_timestamp < :startTimestamp and end_timestamp > :endTimestamp))) as ti " +

                    "inner join task t on t.id = ti.task_id " +
                    "inner join task_type tt on tt.id = t.task_type_id " +
                    "group by tt.id " +
                    "order by total_seconds desc")
    public List<Object[]> byTypeReport(@Param("startTimestamp") Date startTimestamp,
                                       @Param("endTimestamp") Date endTimestamp);
}
