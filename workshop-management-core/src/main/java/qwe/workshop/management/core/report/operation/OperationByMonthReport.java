package qwe.workshop.management.core.report.operation;

import org.joda.time.YearMonth;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * User: artem
 * Date: 09.07.15
 * Time: 2:59
 */
public class OperationByMonthReport implements Serializable {

    private LinkedHashMap<YearMonth, OperationReportAmounts> yearMonthsToAmounts = new LinkedHashMap<>();
    private LinkedHashMap<Integer, OperationReportAmounts> yearsToAmounts = new LinkedHashMap<>();

    public OperationByMonthReport() {

    }

    public void add(YearMonth yearMonth, OperationReportAmounts yearMonthAmounts) {
        yearMonthsToAmounts.put(yearMonth, yearMonthAmounts);

        int year = yearMonth.getYear();
        OperationReportAmounts yearAmounts = yearsToAmounts.get(year);
        yearAmounts = yearAmounts == null ? yearMonthAmounts : yearAmounts.plus(yearMonthAmounts);
        yearsToAmounts.put(year, yearAmounts);
    }

    public LinkedHashMap<YearMonth, OperationReportAmounts> getYearMonthsToAmounts() {
        return yearMonthsToAmounts;
    }

    public LinkedHashMap<Integer, OperationReportAmounts> getYearsToAmounts() {
        return yearsToAmounts;
    }

    @Override
    public String toString() {
        StringBuilder s = new StringBuilder("OperationByMonthReport {yearMonthsToAmounts: [\n");

        for (Map.Entry<YearMonth, OperationReportAmounts> entry : yearMonthsToAmounts.entrySet()) {
            s.append("{yearMonth: ").append(entry.getKey()).append(", amounts: ").append(entry.getValue()).append("}\n");
        }
        s.append("],\nyearsToAmounts:\n");
        for (Map.Entry<Integer, OperationReportAmounts> entry : yearsToAmounts.entrySet()) {
            s.append("{year: ").append(entry.getKey()).append(", amounts: ").append(entry.getValue()).append("}\n");
        }
        s.append("]}");

        return s.toString();
    }
}
