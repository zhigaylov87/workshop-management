package qwe.workshop.management.web.util.exceptionhandler;

import javax.faces.context.ExceptionHandler;
import javax.faces.context.ExceptionHandlerFactory;

/**
 * Overriding only for logging to slf4j
 *
 * User: artem
 * Date: 14.11.13
 * Time: 0:30
 */
public class LoggingFullAjaxExceptionHandlerFactory extends org.omnifaces.exceptionhandler.FullAjaxExceptionHandlerFactory {

    public LoggingFullAjaxExceptionHandlerFactory(ExceptionHandlerFactory wrapped) {
        super(wrapped);
    }

    @Override
    public ExceptionHandler getExceptionHandler() {
        return new LoggingFullAjaxExceptionHandler(super.getWrapped().getExceptionHandler());
    }
}
