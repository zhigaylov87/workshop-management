package qwe.workshop.management.web.component;

/**
 * User: artem
 * Date: 23.04.14
 * Time: 1:46
 */
public interface FilterActionListener {

    public void filteringInvoked();

    public void resetInvoked();

}
