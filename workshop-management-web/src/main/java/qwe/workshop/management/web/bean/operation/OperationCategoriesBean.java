package qwe.workshop.management.web.bean.operation;

import org.primefaces.context.RequestContext;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import qwe.workshop.management.core.domain.operation.OperationCategory;
import qwe.workshop.management.core.domain.operation.OperationType;
import qwe.workshop.management.core.service.ServiceException;
import qwe.workshop.management.core.service.api.OperationCategoryService;
import qwe.workshop.management.web.component.EntityListEditor;
import qwe.workshop.management.web.util.MessageManager;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.faces.context.FacesContext;
import javax.validation.ConstraintViolationException;
import java.io.Serializable;
import java.util.Arrays;
import java.util.List;

/**
 * User: artem
 * Date: 04.07.15
 * Time: 5:57
 */
@Component("operationCategoriesBean")
@Scope("view")
public class OperationCategoriesBean implements EntityListEditor<OperationCategory>, Serializable {

    private static final String BUNDLE_NAME = "qwe.workshop.management.web.i18n.operation.operation_categories";

    @Resource(name = "operationCategoryService")
    private transient OperationCategoryService operationCategoryService;

    @Resource(name = "messageManager")
    private transient MessageManager messageManager;

    private List<OperationType> operationTypes = Arrays.asList(OperationType.values());
    private List<OperationCategory> operationCategories;
    private OperationCategory preparedOperationCategory;
    private String editorDialogTitle;

    @PostConstruct
    private void init() {
        reloadList();
    }

    @Override
    public void prepareToAdd() {
        preparedOperationCategory = new OperationCategory();
        editorDialogTitle = messageManager.getMessage(BUNDLE_NAME, "add_dialog_title");
    }

    @Override
    public void prepareToEdit(OperationCategory entity) {
        preparedOperationCategory = new OperationCategory(entity);
        editorDialogTitle = messageManager.getMessage(BUNDLE_NAME, "edit_dialog_title");
    }

    @Override
    public void save() {
        try {
            preparedOperationCategory = operationCategoryService.save(preparedOperationCategory);
        } catch (ConstraintViolationException e) {
            messageManager.addErrorMessage(e);
            FacesContext.getCurrentInstance().validationFailed();
            return;
        }

        messageManager.addInfoMessage(BUNDLE_NAME, "save_success", preparedOperationCategory.getName());
        reloadList();
        RequestContext.getCurrentInstance().update(Arrays.asList("entityListEditorTableForm", "messages"));
    }

    @Override
    public boolean canBeDelete(OperationCategory entity) {
        return true; // TODO: Проверять, что нет операций с такой категорией, если есть то нельзя удалять
    }

    @Override
    public void prepareToDelete(OperationCategory entity) {
        preparedOperationCategory = entity;
        editorDialogTitle = messageManager.getMessage(BUNDLE_NAME, "delete_dialog_title", preparedOperationCategory.getName());
    }

    @Override
    public void delete() {
        try {
            operationCategoryService.delete(preparedOperationCategory.getId());
        } catch (ServiceException e) {
            messageManager.addErrorMessage(e);
            return;
        }

        messageManager.addInfoMessage(BUNDLE_NAME, "delete_success", preparedOperationCategory.getName());
        reloadList();
    }

    @Override
    public String getEditorDialogTitle() {
        return editorDialogTitle;
    }

    private void reloadList() {
        operationCategories = operationCategoryService.findAll();
    }

    // setters & getters

    public List<OperationCategory> getOperationCategories() {
        return operationCategories;
    }

    public OperationCategory getPreparedOperationCategory() {
        return preparedOperationCategory;
    }

    public List<OperationType> getOperationTypes() {
        return operationTypes;
    }
}
