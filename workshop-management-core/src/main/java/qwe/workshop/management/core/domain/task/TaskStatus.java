package qwe.workshop.management.core.domain.task;

/**
 * User: artem
 * Date: 18.06.15
 * Time: 4:19
 */
public enum TaskStatus {

    PLANNED,
    ACTIVE,
    PAUSED,
    FINISHED

}
