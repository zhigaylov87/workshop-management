package qwe.workshop.management.web.component;

import qwe.workshop.management.core.util.DateInterval;

/**
 * User: artem
 * Date: 08.07.15
 * Time: 5:43
 */
public interface DateIntervalFilter extends FilterActionListener {

    public DateInterval getDateInterval();

}
