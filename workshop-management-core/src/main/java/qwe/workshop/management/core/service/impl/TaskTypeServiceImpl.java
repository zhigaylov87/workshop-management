package qwe.workshop.management.core.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import qwe.workshop.management.core.CoreException;
import qwe.workshop.management.core.domain.task.TaskType;
import qwe.workshop.management.core.repository.TaskTypeRepository;
import qwe.workshop.management.core.service.api.TaskTypeService;

import javax.validation.Valid;
import java.util.List;

/**
 * User: artem
 * Date: 20.06.15
 * Time: 4:23
 */
@Service("taskTypeService")
@Repository
@Transactional
public class TaskTypeServiceImpl implements TaskTypeService {

    private static final Logger log = LoggerFactory.getLogger(TaskTypeServiceImpl.class);

    @Autowired
    private TaskTypeRepository taskTypeRepository;

    @Transactional(readOnly = true)
    @Override
    public List<TaskType> findAll() {
        String logMsg = " of loading all task types";
        try {
            log.info("Start" + logMsg);
            List<TaskType> all = taskTypeRepository.findAll();
            log.info("End" + logMsg + ". Total count: " + all.size());
            return all;
        } catch (Exception e) {
            log.error("Error" + logMsg, e);
            throw new CoreException(e);
        }
    }

    @Override
    public TaskType save(@Valid TaskType taskType) {
        String logMsg = " of saving task type: " + taskType;
        try {
            log.info("Start" + logMsg);
            taskType = taskTypeRepository.save(taskType);
            log.info("End" + logMsg + ". Saved task type: " + taskType);
            return taskType;
        } catch (Exception e) {
            log.error("Error" + logMsg, e);
            throw new CoreException(e);
        }
    }

    @Override
    public void delete(long taskTypeId) {
        String logMsg = " of deleting task type. Task type id: " + taskTypeId;
        try {
            log.info("Start" + logMsg);
            // TODO: Проверять что нет задач с таким типом
            TaskType taskType = taskTypeRepository.findOne(taskTypeId);
            log.info("Processing" + logMsg + ". Task type: " + taskType);
            taskTypeRepository.delete(taskType);
            log.info("End" + logMsg);
        } catch (Exception e) {
            log.error("Error" + logMsg, e);
            throw new CoreException(e);
        }
    }
}
