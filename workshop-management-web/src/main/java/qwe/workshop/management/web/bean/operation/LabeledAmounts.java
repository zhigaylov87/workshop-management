package qwe.workshop.management.web.bean.operation;

import qwe.workshop.management.core.util.FormatUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * User: artem
 * Date: 10.07.15
 * Time: 2:59
 */
public class LabeledAmounts {

    private int maxLength = 0;
    private List<Item> items = new ArrayList<>();

    public void add(String label, int amount, boolean expense) {
        Item item = new Item(label + ": ",
                (expense ? "-" : "+") + " " + FormatUtils.formatAmount(amount),
                amount);

        if (item.length > maxLength) {
            maxLength = item.length;
        }
        items.add(item);
    }

    public List<Item> getItems() {
        return Collections.unmodifiableList(items);
    }

    public class Item {

        private final String label;
        private final String amountStr;
        private final int length;
        private final int amount;

        private Item(String label, String amountStr, int amount) {
            this.label = label;
            this.amountStr = amountStr;
            this.length = label.length() + amountStr.length();
            this.amount = amount;
        }

        public String getFullWidthDescription() {
            int additionalLength = maxLength - length;
            StringBuilder s = new StringBuilder(label);
            for (int i = 0; i < additionalLength; i++) {
                s.append("&nbsp;");
            }
            s.append(amountStr);
            return s.toString();
        }

        public int getAmount() {
            return amount;
        }
    }
}
