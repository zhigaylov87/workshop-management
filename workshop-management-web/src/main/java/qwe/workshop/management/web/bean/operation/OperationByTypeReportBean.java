package qwe.workshop.management.web.bean.operation;

import org.primefaces.model.chart.PieChartModel;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import qwe.workshop.management.core.domain.operation.OperationType;
import qwe.workshop.management.core.report.operation.OperationByTypeReport;
import qwe.workshop.management.core.report.operation.OperationReportAmounts;
import qwe.workshop.management.core.service.api.OperationReportService;
import qwe.workshop.management.core.util.DateHelper;
import qwe.workshop.management.core.util.DateInterval;
import qwe.workshop.management.core.util.EnumHelper;
import qwe.workshop.management.core.util.FormatUtils;
import qwe.workshop.management.web.component.DateIntervalFilter;
import qwe.workshop.management.web.util.Colors;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import java.io.Serializable;

/**
 * User: artem
 * Date: 22.06.15
 * Time: 1:31
 */
@Component("operationByTypeReportBean")
@Scope("view")
public class OperationByTypeReportBean implements DateIntervalFilter, Serializable {

    @Resource(name = "operationReportService")
    private transient OperationReportService operationReportService;

    @Resource(name = "dateHelper")
    private transient DateHelper dateHelper;

    @Resource(name = "enumHelper")
    private transient EnumHelper enumHelper;

    private DateInterval dateInterval;
    private OperationByTypeReport report;
    private PieChartModel pieChartModel;

    @PostConstruct
    private void init() {
        dateInterval = new DateInterval(dateHelper.getCurrentDate());
        reloadReport();
    }

    @Override
    public void filteringInvoked() {
        reloadReport();
    }

    @Override
    public void resetInvoked() {
        throw new UnsupportedOperationException("Reset not supported");
    }

    private void reloadReport() {
        report = operationReportService.getOperationByTypeReport(dateInterval);
        if (pieChartModel == null) {
            pieChartModel = new PieChartModel();
            pieChartModel.setLegendPosition("w");
            pieChartModel.setShowDataLabels(true);
            pieChartModel.setSeriesColors(Colors.getSeriesColors(OperationType.values().length));
        } else {
            pieChartModel.clear();
        }

        OperationReportAmounts amounts = report.getReportAmounts();

        pieChartModel.set(enumHelper.getDescription(OperationType.EXPENSE) +
                ": - " + FormatUtils.formatAmount(amounts.getExpense()) , amounts.getExpense());

        pieChartModel.set(enumHelper.getDescription(OperationType.INCOME) +
                ": + " + FormatUtils.formatAmount(amounts.getIncome()), amounts.getIncome());
    }

    // setter & getters

    @Override
    public DateInterval getDateInterval() {
        return dateInterval;
    }

    public OperationByTypeReport getReport() {
        return report;
    }

    public PieChartModel getPieChartModel() {
        return pieChartModel;
    }
}
