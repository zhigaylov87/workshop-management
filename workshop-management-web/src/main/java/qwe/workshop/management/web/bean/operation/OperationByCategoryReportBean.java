package qwe.workshop.management.web.bean.operation;

import org.primefaces.model.chart.PieChartModel;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import qwe.workshop.management.core.domain.operation.OperationType;
import qwe.workshop.management.core.report.operation.OperationByCategoryReport;
import qwe.workshop.management.core.report.operation.OperationByCategoryReportItem;
import qwe.workshop.management.core.report.operation.OperationByTypeReport;
import qwe.workshop.management.core.service.api.OperationReportService;
import qwe.workshop.management.core.util.DateHelper;
import qwe.workshop.management.core.util.DateInterval;
import qwe.workshop.management.core.util.EnumHelper;
import qwe.workshop.management.web.component.DateIntervalFilter;
import qwe.workshop.management.web.util.Colors;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import java.io.Serializable;
import java.util.List;

/**
 * User: artem
 * Date: 22.06.15
 * Time: 1:31
 */
@Component("operationByCategoryReportBean")
@Scope("view")
public class OperationByCategoryReportBean implements DateIntervalFilter, Serializable {

    @Resource(name = "operationReportService")
    private transient OperationReportService operationReportService;

    @Resource(name = "dateHelper")
    private transient DateHelper dateHelper;

    @Resource(name = "enumHelper")
    private transient EnumHelper enumHelper;


    private DateInterval dateInterval;
    private OperationByTypeReport operationByTypeReport;
    private PieChartModel expensePieChartModel;
    private PieChartModel incomePieChartModel;

    @PostConstruct
    private void init() {
        dateInterval = new DateInterval(dateHelper.getCurrentDate());
        reload();
    }

    @Override
    public void filteringInvoked() {
        reload();
    }

    @Override
    public void resetInvoked() {
        throw new UnsupportedOperationException("Reset not supported");
    }

    private void reload() {
        // by type report
        operationByTypeReport = operationReportService.getOperationByTypeReport(dateInterval);

        // by category report
        OperationByCategoryReport operationByCategoryReport = operationReportService.getOperationByCategoryReport(dateInterval);
        if (expensePieChartModel == null) {
            expensePieChartModel = newPieChartModel(enumHelper.getDescription(OperationType.EXPENSE));
            incomePieChartModel = newPieChartModel(enumHelper.getDescription(OperationType.INCOME));
        } else {
            expensePieChartModel.clear();
            incomePieChartModel.clear();
        }
        setModelData(expensePieChartModel, operationByCategoryReport.getExpenseItems());
        setModelData(incomePieChartModel, operationByCategoryReport.getIncomeItems());
    }

    private void setModelData(PieChartModel model, List<OperationByCategoryReportItem> items) {
        model.setSeriesColors(Colors.getSeriesColors(items.size()));
        LabeledAmounts labeledAmounts = new LabeledAmounts();
        for (OperationByCategoryReportItem item : items) {
            labeledAmounts.add(item.getCategory().getName(), item.getAmount(),
                    item.getCategory().getType() == OperationType.EXPENSE);
        }
        for (LabeledAmounts.Item item : labeledAmounts.getItems()) {
            model.set(item.getFullWidthDescription(), item.getAmount());
        }
    }

    private PieChartModel newPieChartModel(String title) {
        PieChartModel model = new PieChartModel();
        model.setLegendPosition("w");
        model.setShowDataLabels(true);
        model.setTitle(title);
        return model;
    }

    // setters & getters

    @Override
    public DateInterval getDateInterval() {
        return dateInterval;
    }

    public OperationByTypeReport getOperationByTypeReport() {
        return operationByTypeReport;
    }

    public PieChartModel getExpensePieChartModel() {
        return expensePieChartModel;
    }

    public PieChartModel getIncomePieChartModel() {
        return incomePieChartModel;
    }

}
