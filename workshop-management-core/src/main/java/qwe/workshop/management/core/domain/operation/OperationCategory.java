package qwe.workshop.management.core.domain.operation;

import org.hibernate.validator.constraints.NotEmpty;
import qwe.workshop.management.core.domain.Identifiable;
import qwe.workshop.management.core.util.FormatUtils;
import qwe.workshop.management.core.util.ObjectUtils;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;

/**
 * User: artem
 * Date: 01.07.15
 * Time: 2:57
 */
@Entity
@Table(name = "OPERATION_CATEGORY")
public class OperationCategory implements Identifiable, Serializable {

    private Long id;
    private OperationType type;
    private String name;

    public OperationCategory() {
    }

    public OperationCategory(OperationCategory other) {
        this.id = other.id;
        this.type = other.type;
        this.name = other.name;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @NotNull(message = "{OperationCategory.type.NotNull}")
    @Column(name = "TYPE")
    @Enumerated(EnumType.STRING)
    public OperationType getType() {
        return type;
    }

    public void setType(OperationType type) {
        this.type = type;
    }

    @NotEmpty(message = "{OperationCategory.name.NotEmpty}")
    @Size(max = 128, message = "{OperationCategory.name.Size}")
    @Column(name = "NAME", nullable = false)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof OperationCategory)) return false;

        OperationCategory category = (OperationCategory) o;
        return ObjectUtils.equalsIds(this, category);
    }

    @Override
    public int hashCode() {
        return id != null ? id.hashCode() : 0;
    }

    @Override
    public String toString() {
        return "OperationCategory {" +
                "id: " + id +
                ", type: " + type +
                ", name: " + FormatUtils.singleQuotes(name) +
                '}';
    }
}
