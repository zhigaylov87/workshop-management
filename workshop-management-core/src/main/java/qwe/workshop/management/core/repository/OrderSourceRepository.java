package qwe.workshop.management.core.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import qwe.workshop.management.core.domain.operation.OrderSource;

import java.util.List;

/**
 * User: artem
 * Date: 12.07.15
 * Time: 1:19
 */
public interface OrderSourceRepository extends CrudRepository<OrderSource, Long> {

    @Query("select os from OrderSource os order by os.name asc")
    public List<OrderSource> findAll();

}
