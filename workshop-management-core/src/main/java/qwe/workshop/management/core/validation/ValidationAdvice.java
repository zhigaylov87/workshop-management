package qwe.workshop.management.core.validation;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.validation.Validator;
import java.io.Serializable;
import java.util.Arrays;
import java.util.Set;

/**
 * User: artem
 * Date: 25.11.14
 * Time: 0:52
 */
@Component
@Aspect
public class ValidationAdvice implements Serializable {

    private static final Logger log = LoggerFactory.getLogger(ValidationAdvice.class);

    @Resource(name = "validator")
    private Validator validator;

    @Pointcut("execution(* * (@javax.validation.Valid (*), ..)) && args(validationObject, ..)")
    public void validationInvoked(Object validationObject) {}

    @Before(value = "validationInvoked(validationObject)", argNames = "joinPoint, validationObject")
    public void performValidation(JoinPoint joinPoint, Object validationObject) throws ConstraintViolationException {
        Set<ConstraintViolation<Object>> constraintViolations = validator.validate(validationObject);
        if (!constraintViolations.isEmpty()) {
            log.info("Constraint violations: " + Arrays.toString(constraintViolations.toArray()));
            log.info("Invalid entity: " + validationObject);
            throw new ConstraintViolationException(constraintViolations);
        }
    }
}
