package qwe.workshop.management.core.service.api;

import qwe.workshop.management.core.report.task.TaskDurationByTypeReport;
import qwe.workshop.management.core.util.DateInterval;

/**
 * User: artem
 * Date: 11.07.15
 * Time: 3:18
 */
public interface TaskReportService {

    public TaskDurationByTypeReport getDurationByTypeReport(DateInterval interval);

}
