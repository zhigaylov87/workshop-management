package qwe.workshop.management.core.service;

import qwe.workshop.management.core.AbstractTest;
import qwe.workshop.management.core.ResourcesUtils;
import qwe.workshop.management.core.domain.task.Task;
import qwe.workshop.management.core.domain.task.TaskStatus;
import qwe.workshop.management.core.domain.task.TaskType;
import qwe.workshop.management.core.service.api.TaskReportService;
import qwe.workshop.management.core.service.api.TaskService;
import qwe.workshop.management.core.service.api.TaskTypeService;
import qwe.workshop.management.core.util.DateInterval;
import qwe.workshop.management.core.util.MutableDateHelper;

import javax.annotation.Resource;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * User: artem
 * Date: 11.07.15
 * Time: 4:22
 */
public abstract class AbstractServiceTest extends AbstractTest {

    private static final String FOO = "foo";

    @Resource(name = "taskTypeService")
    private TaskTypeService taskTypeService;
    @Resource(name = "taskService")
    private TaskService taskService;
    @Resource(name = "taskReportService")
    private TaskReportService taskReportService;
    @Resource(name = "testDateHelper")
    private MutableDateHelper dateHelper;

    public TaskType newSavedFooTaskType() {
        TaskType taskType = new TaskType();
        taskType.setName(uniqueFoo());
        return taskTypeService.save(taskType);
    }

    public Task newSavedFooTask(TaskType type) {
        Task task = new Task();
        task.setType(type);
        task.setName(uniqueFoo());
        task.setStatus(TaskStatus.PLANNED);
        task.setStatusTimestamp(dateHelper.getCurrentDate());
        return taskService.save(task);
    }

    /**
     * @param dateStr date with format {@link ResourcesUtils#TIMESTAMP_PATTERN}
     * @return current date that was set
     */
    public Date setCurrentDate(String dateStr) {
        Date date = parseDate(dateStr);
        dateHelper.setDate(date);
        return date;
    }

    public DateInterval newDateInterval(String startStr, String endStr) {
        return new DateInterval(parseDate(startStr), parseDate(endStr));
    }

    private String uniqueFoo() {
        return FOO + System.currentTimeMillis();
    }

    private Date parseDate(String dateStr) {
        try {
            return new SimpleDateFormat(ResourcesUtils.TIMESTAMP_PATTERN).parse(dateStr);
        } catch (ParseException e) {
            throw new RuntimeException(e);
        }
    }
}
