package qwe.workshop.management.core.util;

import java.util.Date;

/**
 * User: artem
 * Date: 25.06.15
 * Time: 3:08
 */
public interface DateHelper {

    public Date getCurrentDate();

}
