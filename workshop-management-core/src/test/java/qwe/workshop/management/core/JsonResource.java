package qwe.workshop.management.core;

/**
 * User: artem
 * Date: 18.07.15
 * Time: 5:03
 */
public enum JsonResource {

    ValidTaskStatusToStatusChanges("/qwe/workshop/management/core/service/ValidTaskStatusToStatusChanges.json"),
    ValidTaskTimeDurations("/qwe/workshop/management/core/service/ValidTaskTimeDurations.json"),
    ValidFormatTimeDurations("/qwe/workshop/management/core/util/ValidFormatTimeDurations.json");

    private final String path;

    private JsonResource(String path) {
        this.path = path;
    }

    public String getPath() {
        return path;
    }
}
