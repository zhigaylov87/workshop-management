package qwe.workshop.management.core.util;

import qwe.workshop.management.core.domain.Identifiable;

import java.util.*;

/**
 * User: artem
 * Date: 27.04.14
 * Time: 6:24
 */
public class IdentifiableList<T extends Identifiable> {

    private List<T> all;
    private Map<Long, T> idToElement = new HashMap<>();

    public IdentifiableList(Collection<T> all) {
        this.all = Collections.unmodifiableList(new ArrayList<>(all));
        for (T element : all) {
            idToElement.put(element.getId(), element);
        }
    }

    public T getFirst() {
        return all.isEmpty() ? null : all.get(0);
    }

    public T getLast() {
        return all.isEmpty() ? null : all.get(all.size() - 1);
    }

    public List<T> getAll() {
        return all;
    }

    /**
     * @param id element id
     * @return element with defined id
     * @throws IllegalArgumentException if element with defined id not found in list
     */
    public T getById(long id) throws IllegalArgumentException {
        T element = idToElement.get(id);
        if (element == null) {
            throw new IllegalArgumentException("Element with id = " + id + " not found!");
        }
        return element;
    }

    public void checkContainsById(long id) throws NoSuchElementException {
        if (!containsById(id)) {
            throw new NoSuchElementException("Element with id = " + id + " not found!");
        }
    }

    public boolean containsById(long id) {
        return idToElement.get(id) != null;
    }

    public List<T> getListByIds(List<Long> ids) {
        List<T> list = new ArrayList<>();
        for (long id : ids) {
            list.add(getById(id));
        }
        return Collections.unmodifiableList(list);
    }

    public Set<T> getSetByIds(Set<Long> ids) {
        return new HashSet<>(getListByIds(new ArrayList<>(ids)));
    }
}
