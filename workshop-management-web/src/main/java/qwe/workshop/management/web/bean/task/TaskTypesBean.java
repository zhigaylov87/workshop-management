package qwe.workshop.management.web.bean.task;

import org.primefaces.context.RequestContext;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import qwe.workshop.management.core.domain.task.TaskType;
import qwe.workshop.management.core.service.ServiceException;
import qwe.workshop.management.core.service.api.TaskTypeService;
import qwe.workshop.management.web.component.EntityListEditor;
import qwe.workshop.management.web.util.MessageManager;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.faces.context.FacesContext;
import javax.validation.ConstraintViolationException;
import java.io.Serializable;
import java.util.Arrays;
import java.util.List;

/**
 * User: artem
 * Date: 21.06.15
 * Time: 3:18
 */
@Component("taskTypesBean")
@Scope("view")
public class TaskTypesBean implements EntityListEditor<TaskType>, Serializable {

    private static final String BUNDLE_NAME = "qwe.workshop.management.web.i18n.task.task_types";

    @Resource(name = "taskTypeService")
    private transient TaskTypeService taskTypeService;

    @Resource(name = "messageManager")
    private transient MessageManager messageManager;

    private List<TaskType> taskTypes;
    private TaskType preparedTaskType;
    private String editorDialogTitle;

    @PostConstruct
    private void init() {
        reloadList();
    }

    @Override
    public void prepareToAdd() {
        preparedTaskType = new TaskType();
        editorDialogTitle = messageManager.getMessage(BUNDLE_NAME, "add_dialog_title");
    }

    @Override
    public void prepareToEdit(TaskType entity) {
        preparedTaskType = new TaskType(entity);
        editorDialogTitle = messageManager.getMessage(BUNDLE_NAME, "edit_dialog_title");
    }

    @Override
    public boolean canBeDelete(TaskType entity) {
        return true; // TODO: Проверять, что нет задач с таким статусом, если есть то нельзя удалять
    }

    @Override
    public void prepareToDelete(TaskType entity) {
        preparedTaskType = entity;
        editorDialogTitle = messageManager.getMessage(BUNDLE_NAME, "delete_dialog_title", preparedTaskType.getName());
    }

    @Override
    public String getEditorDialogTitle() {
        return editorDialogTitle;
    }

    @Override
    public void save() {
        try {
            preparedTaskType = taskTypeService.save(preparedTaskType);
        } catch (ConstraintViolationException e) {
            messageManager.addErrorMessage(e);
            FacesContext.getCurrentInstance().validationFailed();
            return;
        }

        messageManager.addInfoMessage(BUNDLE_NAME, "save_success", preparedTaskType.getName());
        reloadList();
        RequestContext.getCurrentInstance().update(Arrays.asList("entityListEditorTableForm", "messages"));
    }

    @Override
    public void delete() {
        try {
            taskTypeService.delete(preparedTaskType.getId());
        } catch (ServiceException e) {
            messageManager.addErrorMessage(e);
            return;
        }

        messageManager.addInfoMessage(BUNDLE_NAME, "delete_success", preparedTaskType.getName());
        reloadList();
    }

    private void reloadList() {
        taskTypes = taskTypeService.findAll();
    }

    // setters & getters

    public List<TaskType> getTaskTypes() {
        return taskTypes;
    }

    public TaskType getPreparedTaskType() {
        return preparedTaskType;
    }

}
