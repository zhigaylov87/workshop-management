package qwe.workshop.management.web.bean.operation;

import org.joda.time.DateTime;
import org.joda.time.YearMonth;
import org.primefaces.model.chart.AxisType;
import org.primefaces.model.chart.CategoryAxis;
import org.primefaces.model.chart.LineChartModel;
import org.primefaces.model.chart.LineChartSeries;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import qwe.workshop.management.core.report.operation.OperationByMonthReport;
import qwe.workshop.management.core.report.operation.OperationReportAmounts;
import qwe.workshop.management.core.service.api.OperationReportService;
import qwe.workshop.management.core.util.DateHelper;
import qwe.workshop.management.web.util.MessageManager;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import java.io.Serializable;
import java.util.*;

/**
 * User: artem
 * Date: 22.06.15
 * Time: 1:31
 */
@Component("operationByMonthReportBean")
@Scope("view")
public class OperationByMonthReportBean implements Serializable {

    @Resource(name = "operationReportService")
    private transient OperationReportService operationReportService;

    @Resource(name = "messageManager")
    private transient MessageManager messageManager;

    @Resource(name = "dateHelper")
    private transient DateHelper dateHelper;

    private List<Integer> operationYears;
    private List<Integer> selectedYears;
    private OperationByMonthReport report;
    private LineChartModel chartModel;

    @PostConstruct
    private void init() {
        operationYears = operationReportService.getOperationYears();
        int currentYear = new DateTime(dateHelper.getCurrentDate()).getYear();
        int selectedYear =
                operationYears.isEmpty() ? currentYear :
                operationYears.contains(currentYear) ? currentYear :
                        operationYears.get(operationYears.size() - 1);
        selectedYears = Arrays.asList(selectedYear);
        reloadReport();
    }

    public void reloadReport() {
        report = operationReportService.getOperationByMonthReport(new HashSet<>(selectedYears));

        chartModel = new LineChartModel();
        chartModel.getAxis(AxisType.Y).setLabel(messageManager.getCommon("amount"));
        chartModel.setSeriesColors("B6B8B7,FCC372,008100");
        chartModel.setLegendPosition("e");
        chartModel.setAnimate(true);
        CategoryAxis axis = new CategoryAxis();
        chartModel.getAxes().put(AxisType.X, axis);

        LineChartSeries expenseSeries = new LineChartSeries();
        expenseSeries.setLabel(messageManager.getCommon("expense"));
        LineChartSeries incomeSeries = new LineChartSeries();
        incomeSeries.setLabel(messageManager.getCommon("income"));
        LineChartSeries profitSeries = new LineChartSeries();
        profitSeries.setLabel(messageManager.getCommon("profit"));

        for (Map.Entry<YearMonth, OperationReportAmounts> entry : report.getYearMonthsToAmounts().entrySet()) {
            YearMonth yearMonth = entry.getKey();
            String yearMonthStr = messageManager.getMonthName(yearMonth.getMonthOfYear()) + " " + yearMonth.getYear();
            OperationReportAmounts amounts = entry.getValue();

            expenseSeries.set(yearMonthStr, amounts.getExpense());
            incomeSeries.set(yearMonthStr, amounts.getIncome());
            profitSeries.set(yearMonthStr, amounts.getProfit());
        }

        chartModel.addSeries(expenseSeries);
        chartModel.addSeries(incomeSeries);
        chartModel.addSeries(profitSeries);
    }

    // setters & getters

    public LineChartModel getChartModel() {
        return chartModel;
    }

    public List<YearMonth> getReportYearMonths() {
        return new ArrayList<>(report.getYearMonthsToAmounts().keySet());
    }

    public OperationReportAmounts getReportYearMonthAmounts(YearMonth yearMonth) {
        return report.getYearMonthsToAmounts().get(yearMonth);
    }

    public List<Integer> getReportYears() {
        return new ArrayList<>(report.getYearsToAmounts().keySet());
    }

    public OperationReportAmounts getReportYearAmounts(int year) {
        return report.getYearsToAmounts().get(year);
    }

    public List<Integer> getOperationYears() {
        return operationYears;
    }

    public List<Integer> getSelectedYears() {
        return selectedYears;
    }

    public void setSelectedYears(List<Integer> selectedYears) {
        this.selectedYears = selectedYears;
    }
}
