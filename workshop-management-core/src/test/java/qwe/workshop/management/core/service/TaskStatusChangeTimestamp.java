package qwe.workshop.management.core.service;

import com.fasterxml.jackson.annotation.JsonFormat;
import qwe.workshop.management.core.ResourcesUtils;
import qwe.workshop.management.core.domain.task.TaskStatus;

import java.util.Date;

/**
 * User: artem
 * Date: 18.07.15
 * Time: 7:50
 */
public class TaskStatusChangeTimestamp {

    private TaskStatus status;
    private Date timestamp;

    public TaskStatus getStatus() {
        return status;
    }

    public void setStatus(TaskStatus status) {
        this.status = status;
    }

    @JsonFormat(pattern = ResourcesUtils.TIMESTAMP_PATTERN)
    public Date getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }
}
