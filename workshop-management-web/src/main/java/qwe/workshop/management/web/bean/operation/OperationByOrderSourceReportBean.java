package qwe.workshop.management.web.bean.operation;

import org.primefaces.model.chart.PieChartModel;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import qwe.workshop.management.core.domain.operation.OrderSource;
import qwe.workshop.management.core.report.operation.OperationByOrderSourceReport;
import qwe.workshop.management.core.report.operation.OperationByOrderSourceReportItem;
import qwe.workshop.management.core.service.api.OperationReportService;
import qwe.workshop.management.core.util.DateHelper;
import qwe.workshop.management.core.util.DateInterval;
import qwe.workshop.management.core.util.FormatUtils;
import qwe.workshop.management.web.component.DateIntervalFilter;
import qwe.workshop.management.web.util.Colors;
import qwe.workshop.management.web.util.MessageManager;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import java.io.Serializable;
import java.util.List;

/**
 * User: artem
 * Date: 22.06.15
 * Time: 1:31
 */
@Component("operationByOrderSourceReportBean")
@Scope("view")
public class OperationByOrderSourceReportBean implements DateIntervalFilter, Serializable {

    @Resource(name = "operationReportService")
    private transient OperationReportService operationReportService;

    @Resource(name = "dateHelper")
    private transient DateHelper dateHelper;

    @Resource(name = "messageManager")
    private transient MessageManager mm;

    private DateInterval dateInterval;
    private OperationByOrderSourceReport report;
    private PieChartModel pieChartModel;

    @PostConstruct
    private void init() {
        dateInterval = new DateInterval(dateHelper.getCurrentDate());
        reloadReport();
    }

    @Override
    public void filteringInvoked() {
        reloadReport();
    }

    @Override
    public void resetInvoked() {
        throw new UnsupportedOperationException("Reset not supported");
    }

    private void reloadReport() {
        report = operationReportService.getOperationByOrderSourceReport(dateInterval);
        if (pieChartModel == null) {
            pieChartModel = new PieChartModel();
            pieChartModel.setLegendPosition("w");
            pieChartModel.setShowDataLabels(true);
        } else {
            pieChartModel.clear();
        }

        List<OperationByOrderSourceReportItem> items = report.getItems();
        pieChartModel.setSeriesColors(Colors.getSeriesColors(items.size()));
        for (OperationByOrderSourceReportItem item : items) {
            OrderSource orderSource = item.getOrderSource();
            pieChartModel.set(
                    (orderSource == null ? mm.getCommon("not_defined") : orderSource.getName()) + ": " +
                            FormatUtils.formatAmount(item.getTotalIncome()) +
                            " (" + mm.getCommon("count_short") + ": " + item.getOperationsCount() + ")",
                    item.getTotalIncome());
        }
    }

    // setter & getters

    @Override
    public DateInterval getDateInterval() {
        return dateInterval;
    }

    public OperationByOrderSourceReport getReport() {
        return report;
    }

    public PieChartModel getPieChartModel() {
        return pieChartModel;
    }
}
