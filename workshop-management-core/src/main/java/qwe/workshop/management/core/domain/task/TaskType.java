package qwe.workshop.management.core.domain.task;

import org.hibernate.validator.constraints.NotEmpty;
import qwe.workshop.management.core.domain.Identifiable;
import qwe.workshop.management.core.util.FormatUtils;
import qwe.workshop.management.core.util.ObjectUtils;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.io.Serializable;

/**
 * User: artem
 * Date: 18.06.15
 * Time: 3:59
 */
@Entity
@Table(name = "TASK_TYPE", uniqueConstraints = {@UniqueConstraint(columnNames = "NAME")})
public class TaskType implements Identifiable, Serializable {

    private Long id;
    private String name;

    public TaskType() {
    }

    public TaskType(TaskType other) {
        this.id = other.id;
        this.name = other.name;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @NotEmpty(message = "{TaskType.name.NotEmpty}")
    @Size(max = 128, message = "{TaskType.name.Size}")
    @Column(name = "NAME", unique = true, nullable = false)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof TaskType)) return false;

        TaskType taskType = (TaskType) o;
        return ObjectUtils.equalsIds(this, taskType);
    }

    @Override
    public int hashCode() {
        return id != null ? id.hashCode() : 0;
    }

    @Override
    public String toString() {
        return "TaskType {" +
                "id: " + id +
                ", name: " + FormatUtils.singleQuotes(name) +
                '}';
    }
}
