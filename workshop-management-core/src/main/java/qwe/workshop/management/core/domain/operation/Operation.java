package qwe.workshop.management.core.domain.operation;

import qwe.workshop.management.core.domain.Identifiable;
import qwe.workshop.management.core.util.FormatUtils;
import qwe.workshop.management.core.util.ObjectUtils;
import qwe.workshop.management.core.validation.constraint.OperationConstraint;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Date;

/**
 * User: artem
 * Date: 01.07.15
 * Time: 3:00
 */
@Entity
@Table(name = "OPERATION")
@OperationConstraint
public class Operation implements Identifiable, Serializable {

    private Long id;
    private Date date;
    private OperationCategory category;
    private OrderSource orderSource;
    private Integer amount;
    private String comment;

    public Operation() {
    }

    public Operation(Operation other) {
        this.id = other.id;
        this.date = other.date;
        this.category = other.category;
        this.orderSource = other.orderSource;
        this.amount = other.amount;
        this.comment = other.comment;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @NotNull(message = "{Operation.date.NotNull}")
    @Column(name = "OPERATION_DATE")
    @Temporal(TemporalType.DATE)
    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    @NotNull(message = "{Operation.category.NotNull}")
    @ManyToOne(optional = false)
    @JoinColumn(name = "OPERATION_CATEGORY_ID")
    public OperationCategory getCategory() {
        return category;
    }

    public void setCategory(OperationCategory category) {
        this.category = category;
    }

    @ManyToOne
    @JoinColumn(name = "ORDER_SOURCE_ID")
    public OrderSource getOrderSource() {
        return orderSource;
    }

    public void setOrderSource(OrderSource orderSource) {
        this.orderSource = orderSource;
    }

    @NotNull(message = "{Operation.amount.NotNull}")
    @Column(name = "AMOUNT")
    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    @Size(max = 512, message = "{Operation.comment.Size}")
    @Column(name = "COMMENT")
    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Operation)) return false;

        Operation operation = (Operation) o;
        return ObjectUtils.equalsIds(this, operation);
    }

    @Override
    public int hashCode() {
        return id != null ? id.hashCode() : 0;
    }

    @Override
    public String toString() {
        return "Operation {" +
                "id: " + id +
                ", date: " + date +
                ", category: " + category +
                ", orderSource: " + orderSource +
                ", amount: " + amount +
                ", comment: " + FormatUtils.singleQuotes(comment) +
                '}';
    }
}
