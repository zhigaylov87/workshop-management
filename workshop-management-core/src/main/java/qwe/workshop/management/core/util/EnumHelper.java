package qwe.workshop.management.core.util;

import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;

/**
 * User: artem
 * Date: 07.06.14
 * Time: 23:03
 */
public class EnumHelper {

    private Map<Class<? extends Enum>, ResourceBundle> enumClassToResourceBundle;

    public <T extends Enum> String getDescription(T enumElement) {
        return getDescription(enumElement, null);
    }

    public <T extends Enum> String getDescription(T enumElement, String suffix) {
        ResourceBundle resourceBundle = enumClassToResourceBundle.get(enumElement.getClass());
        if (resourceBundle == null) {
            throw new IllegalArgumentException("There is no resource bundle defined for: " + enumElement.getClass().getName());
        }
        return resourceBundle.getString(enumElement.name() + (suffix == null ? "" : suffix));
    }

    public void setEnumClassToResourceBundleName(Map<Class<? extends Enum>, String> enumClassToResourceBundleName) {
        enumClassToResourceBundle = new HashMap<>();

        for (Map.Entry<Class<? extends Enum>, String> entry : enumClassToResourceBundleName.entrySet()) {
            enumClassToResourceBundle.put(entry.getKey(), ResourceBundle.getBundle(entry.getValue()));
        }
    }
}
