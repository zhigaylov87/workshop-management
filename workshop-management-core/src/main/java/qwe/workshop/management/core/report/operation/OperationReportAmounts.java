package qwe.workshop.management.core.report.operation;

import java.io.Serializable;

/**
 * User: artem
 * Date: 09.07.15
 * Time: 3:23
 */
public final class OperationReportAmounts implements Serializable {

    private int expense;
    private int income;
    private int profit;

    public OperationReportAmounts(int expense, int income) {
        this.expense = expense;
        this.income = income;
        this.profit = income - expense;
    }

    public int getExpense() {
        return expense;
    }

    public int getIncome() {
        return income;
    }

    public int getProfit() {
        return profit;
    }

    public OperationReportAmounts plus(OperationReportAmounts amounts) {
        return new OperationReportAmounts(expense + amounts.expense, income + amounts.income);
    }

    @Override
    public String toString() {
        return "OperationReportAmounts {" +
                "expense: " + expense +
                ", income: " + income +
                ", profit: " + profit +
                '}';
    }
}
